/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.features.shared.customview;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.v4.view.ViewPager;
import android.widget.ImageView;

import com.sales1crm.ekamant.sales1crm.R;

/**
 * Created by Dea Synthia on 1/5/2017.
 */

public class CustomProgressDialog extends Dialog {

    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus){
        ImageView imageView = (ImageView) findViewById(R.id.ivLoading);
        AnimationDrawable yourAnimation = (AnimationDrawable) imageView.getBackground();
        yourAnimation.start();
    }

    public static CustomProgressDialog createDialog(Context context,String title,String message){
        CustomProgressDialog dialog=null;
        if(title.length()==0||title==null)
            dialog=new CustomProgressDialog(context,R.style.CustomProgressDialog);
        else{
            dialog=new CustomProgressDialog(context,R.style.CustomProgressDialog);
            dialog.setTitle(title);
        }

        dialog.setContentView(R.layout.loading_dialog);
        dialog.getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
//        dialog.getWindow().getAttributes().gravity=Gravity.CENTER;
        return dialog;
    }
}