/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.network.api;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.sales1crm.ekamant.sales1crm.utils.Ekamant;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;

public class EkamantApiService extends Service {
	private final static String TAG = EkamantApiService.class.getSimpleName();
	
	private final Messenger mService = new Messenger(new ServiceHandler(this));
	private ApiRequest apiRequest;

	@Override
	public void onCreate() {
		super.onCreate();
		apiRequest = new ApiRequest();
		Log.i(TAG, "service oncreate");
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return mService.getBinder();
	}

	static class ServiceHandler extends Handler {
		WeakReference<EkamantApiService> screen;

		public ServiceHandler(EkamantApiService myScreen) {
			screen = new WeakReference<EkamantApiService>(myScreen);
		}

		@Override
		public void handleMessage(Message msg) {
			Log.i(TAG, "screen : " + screen);
			Log.i(TAG, "MESSAGE " + msg.getData());
			screen.get().runAPIRequest(msg);
		}
	}

	public void runAPIRequest(Message msg) {
		Log.i(TAG, "masuk runAPIRequest");
		final int apiIndex = msg.what;
		final Bundle bundle = msg.getData();
		final Messenger mReplyTo = msg.replyTo;

		new Thread(new Runnable() {

			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				Log.i(TAG, "Response " + bundle.getSerializable("parameters"));
				Log.i(TAG, "bundle json " + bundle.getString("json"));
				HashMap<String, String> parameters = (HashMap<String, String>) bundle
						.getSerializable("parameters");
				HashMap<Object, Object> response = null;
				if (bundle.getInt("type") == Ekamant.POST_WITH_JSON_OBJECT) {
					JSONObject object = null;
					try {
						object = new JSONObject(bundle.getString("json"));
					} catch (JSONException e) {
						e.printStackTrace();
					}
					response = apiRequest.getAPIRequest(apiIndex, object,
							parameters, bundle.getBoolean("MultiEntity"),
							bundle.getBundle("bundle"));
				} else if (bundle.getBoolean("for_httpost")) {
					response = apiRequest.getAPIRequest(apiIndex, parameters,
							bundle.getBoolean("MultiEntity"),
							bundle.getBundle("bundle"), bundle.getBoolean("for_httpost"));
				} else {
					response = apiRequest.getAPIRequest(apiIndex, parameters,
							bundle.getBoolean("MultiEntity"),
							bundle.getBundle("bundle"));
				}
				Message message = Message.obtain(null, apiIndex);
				Bundle bMessage = new Bundle();
				bMessage.putSerializable("response", response);
				message.setData(bMessage);
				try {
					mReplyTo.send(message);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}
}