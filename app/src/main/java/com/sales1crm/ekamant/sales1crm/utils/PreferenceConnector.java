/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * This class handle all preference connection.
 */

public class PreferenceConnector {

    private final static String TAG = PreferenceConnector.class.getSimpleName();

    public final static String PREF_NAME = "PEOPLE_PREFERENCE";
    public final static int MODE = Context.MODE_PRIVATE;

    public static final String API_KEY = "API_KEY";
    public static final String USER_ID = "USER_ID";
    public static final String USER_NAME = "USER_NAME";

    public static final String URL = "URL";
    public static final String ENCODED_PASSCODE = "ENCODED_PASSCODE";
    public static final String PASSCODE = "PASSCODE";
    public static final String CHECKIN = "CHECKIN";
    public static final String PHOTO = "PHOTO";
    public static final String PHOTO_PATH1 = "PHOTO_PATH1";
    public static final String PHOTO_PATH2 = "PHOTO_PATH2";
    public static final String PHOTO_PATH3 = "PHOTO_PATH3";
    public static final String TASKLIST_TYPE ="TASKLIST_TYPE";
    public static final String PREFERENCES_PROPERTY_REG_ID = "PREFERENCES_PROPERTY_REG_ID";
    public static final String PREFERENCES_DEVICE_TOKEN = "PREFERENCES_DEVICE_TOKEN";
    public static final String TASK_ID = "TASK_ID";

    //START AND END WORKING HOUR
    public static final String START_HOUR = "START_HOUR";
    public static final String START_MINUTE = "START_MINUTE";
    public static final String END_HOUR = "END_HOUR";
    public static final String END_MINUTE = "END_MINUTE";

    public static final String DRIVER_STAFF = "DRIVER_STAFF";//1 staff, 2 driver


    public static final String REMINDME = "REMINDME";
    public static final String WORK_START_NOTIF = "WORK_START_NOTIF";
    public static final String WORK_END_NOTIF = "WORK_END_NOTIF";

    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public static boolean readBoolean(Context context, String key, boolean value) {
        return getPreferences(context).getBoolean(key, value);
    }

    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();
    }

    public static String readString(Context context, String key, String value) {
        return getPreferences(context).getString(key, value);
    }

    public static void writeInt(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();
    }

    public static int readInt(Context context, String key, int value) {
        return getPreferences(context).getInt(key, value);
    }

}
