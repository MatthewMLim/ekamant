/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.utils;

import android.content.Context;
import android.content.Intent;

import java.util.Date;

/**
 * TODO: Add a class Header comment!
 */

public class Ekamant {

    public static Context context = null;
    public static Intent iService = null;
    public static Intent offlineService = null;
    public static String directory;
    public static int POST_WITH_JSON_OBJECT = 1;
    public static int POST_WITH_JSON_ARRAY = 1;

    public static Date today_date;
    public static boolean service_running = false;


    // Default value
    public static final int PENDINGINTENT_LATEALARM = 99990;
    public static final int PENDINGINTENT_STARTALARM = 99993;
    public static final int PENDINGINTENT_ENDALARM = 99991;
    public static final int PENDINGINTENT_LOCATION = 99999;
    public static final int DEFAULT_VALUEFORCHECK_IN = 1;
    public static final int CHECKOUT_ALREADY = 0;

    // DIALOG
    public static final int TYPE_OK = 1;
    public static final int TYPE_YESNO = 2;


    public static final int CAMERA_CAPTURE = 2;
    public static final int GALLERY = 3;
    public static final int BACK_FROM_ADD = 4;
    public static final int TYPE_RECT = 0;
    public static final int TYPE_ROUND = 1;

    //	FireBase
    public static final String TOPIC_GLOBAL = "global";
    //	broadcast receiver intent filter
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    //  id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;


}
