/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.features.verification;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.sales1crm.ekamant.sales1crm.R;
import com.sales1crm.ekamant.sales1crm.app.EkamantApplication;
import com.sales1crm.ekamant.sales1crm.database.task.Task;
import com.sales1crm.ekamant.sales1crm.database.task.TaskDataSource;
import com.sales1crm.ekamant.sales1crm.features.finish.ActivityFinish;
import com.sales1crm.ekamant.sales1crm.features.shared.customview.CustomProgressDialog;
import com.sales1crm.ekamant.sales1crm.utils.Ekamant;
import com.sales1crm.ekamant.sales1crm.utils.GpsTracker;
import com.sales1crm.ekamant.sales1crm.utils.PreferenceConnector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Add a class Header comment!
 */

public class SignatureActivity extends AppCompatActivity {

    private static final String TAG = SignatureActivity.class.getSimpleName();
    public static String tempDir;
    private String uniqueId;
    private String sign;
    public String current = null, signature_path;

    private TextView cancel, clear, getsign;
    private RelativeLayout rlBack;

    File mypath;

    private Context mContext;

    SignaturePad mContent;

    TaskDataSource taskDataSource;
    Task task;

    private CustomProgressDialog mLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_signature);
        this.mContext = this;
        Ekamant.context = this;

        mLoading = CustomProgressDialog.createDialog(this, "", "");
        mLoading.setCancelable(false);

        taskDataSource = new TaskDataSource(mContext);

        tempDir = Environment.getExternalStorageDirectory() + "/"
                + getResources().getString(R.string.external_dir) + "/";
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir(
                getResources().getString(R.string.external_dir),
                Context.MODE_PRIVATE);

        prepareDirectory();
        uniqueId = getTodaysDate() + "_" + getCurrentTime() + "_"
                + Math.random();
        current = uniqueId + ".png";
        mypath = new File(directory, current);

        initView();
    }

    private void initView() {
        rlBack = (RelativeLayout) findViewById(R.id.rlBack);
        mContent = (SignaturePad) findViewById(R.id.signature_pad);
        clear = (TextView) findViewById(R.id.clear);
        clear.setEnabled(false);
        getsign = (TextView) findViewById(R.id.getsign);
        getsign.setEnabled(false);
        cancel = (TextView) findViewById(R.id.cancel);

        mContent.setOnSignedListener(new SignaturePad.OnSignedListener() {

            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                getsign.setEnabled(true);
                clear.setEnabled(true);
            }

            @Override
            public void onClear() {
                getsign.setEnabled(false);
                clear.setEnabled(false);
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("log_tag", "Panel Cleared");
                // mSignature.clear();
                mContent.clear();
                getsign.setEnabled(false);
            }
        });

        getsign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bitmap signatureBitmap = mContent.getSignatureBitmap();
                if (addSignatureToGallery(signatureBitmap)) {

                    sign = encodeToBase64(signatureBitmap, Bitmap.CompressFormat.JPEG, 100);
                    Toast.makeText(mContext, "Signature saved into the Gallery",
                            Toast.LENGTH_SHORT).show();
                    postData();
                } else {
                    Toast.makeText(mContext, "Unable to store the signature",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("TAG", "Panel Canceled");
                finish();
            }
        });

        rlBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    private boolean makedirs() {
        File tempdir = new File(tempDir);
        if (!tempdir.exists())
            tempdir.mkdirs();

        if (tempdir.isDirectory()) {
            File[] files = tempdir.listFiles();
            for (File file : files) {
                if (!file.delete()) {
                    System.out.println("Failed to delete " + file);
                }
            }
        }
        return (tempdir.isDirectory());
    }

    private boolean prepareDirectory() {
        try {
            if (makedirs()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(
                    this,
                    "Could not initiate File System.. Is Sdcard mounted properly?",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private String getTodaysDate() {

        final Calendar c = Calendar.getInstance();
        int todaysDate = (c.get(Calendar.YEAR) * 10000)
                + ((c.get(Calendar.MONTH) + 1) * 100)
                + (c.get(Calendar.DAY_OF_MONTH));
        Log.w("DATE:", String.valueOf(todaysDate));
        return (String.valueOf(todaysDate));

    }

    private String getCurrentTime() {

        final Calendar c = Calendar.getInstance();
        int currentTime = (c.get(Calendar.HOUR_OF_DAY) * 10000)
                + (c.get(Calendar.MINUTE) * 100) + (c.get(Calendar.SECOND));
        Log.w("TIME:", String.valueOf(currentTime));
        return (String.valueOf(currentTime));

    }

    public boolean addSignatureToGallery(Bitmap signature) {
        /*String hour = "", minute = "", second = "", lon = "", lat = "";
        Calendar calendar = Calendar.getInstance();
        hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        minute = String.valueOf(calendar.get(Calendar.MINUTE));
        second = String.valueOf(calendar.get(Calendar.SECOND));
        GpsTracker gpsTracker = new GpsTracker(mContext);
        if (gpsTracker.canGetLocation()) {
            lon = String.valueOf(gpsTracker.latitude);

            lat = String.valueOf(gpsTracker.longitude);

            Log.i("AAA", "longitude : " + lon + ", latituded : " + lat);
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }*/

        boolean result = false;
        try {
            File photo = new File(getAlbumStorageDir("SignaturePad"),
                    String.format("sign.jpg"));
            saveBitmapToJPG(signature, photo);
            Intent mediaScanIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(photo);
            signature_path = contentUri.getPath();
            Log.i("TAG", "contenturi : " + contentUri.getPath());
            mediaScanIntent.setData(contentUri);
            SignatureActivity.this.sendBroadcast(mediaScanIntent);
            result = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                albumName);
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created");
        }
        return file;
    }

    public void saveBitmapToJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    private void postData() {
        mLoading.show();
        taskDataSource.open();
        final long count = taskDataSource.countDb();
        Log.d(TAG, "postData: " + String.valueOf(count));
        task = taskDataSource.getSingleTask(String.valueOf(count));
        Log.d(TAG, "task: " + task.getLatitude() + " " + task.getLongitude() + " " + task
                .getOrder_status() + "| " + task.getCreated_at());

        StringRequest postRequest = new StringRequest(Request.Method.POST,
                "http://ekamant.sales1crm.com/api/crm-task/test",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, response.toString());
                        mLoading.dismiss();

                        try {
                            JSONObject result = new JSONObject(response);

                            if (result.get("result").equals("OK")) {

                                PreferenceConnector.writeString(mContext, PreferenceConnector
                                        .PHOTO, "");
                                PreferenceConnector.writeString(mContext, PreferenceConnector
                                        .PHOTO_PATH1, "");
                                PreferenceConnector.writeString(mContext, PreferenceConnector
                                        .PHOTO_PATH2, "");
                                PreferenceConnector.writeString(mContext, PreferenceConnector
                                        .PHOTO_PATH3, "");

                                startActivity(new Intent(SignatureActivity.this, ActivityFinish.class));
                                finish();
                            } else if (result.get("result").equals("NG")) {
                                Toast.makeText(mContext, "Silahkan Coba lagi", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:
                params.put("api_key", PreferenceConnector.readString(
                        mContext, PreferenceConnector.API_KEY, ""));
                params.put("account_id", "43");
                params.put("latitude", task.getLatitude());
                params.put("longitude", task.getLongitude());
                params.put("notes", task.getNotes());

                if (!PreferenceConnector.readString(mContext,
                        PreferenceConnector.PHOTO_PATH1, "").equalsIgnoreCase("")) {
                    params.put("nama1", "photo1_" + count + ".jpg");
                    params.put("photo_1",
                            PreferenceConnector.readString(mContext,
                                    PreferenceConnector.PHOTO_PATH1, ""));
                }
                if (!PreferenceConnector.readString(mContext,
                        PreferenceConnector.PHOTO_PATH2, "").equalsIgnoreCase("")) {
                    params.put("nama2", "photo2_" + count + ".jpg");
                    params.put("photo_2",
                            PreferenceConnector.readString(mContext,
                                    PreferenceConnector.PHOTO_PATH2, ""));
                }
                if (!PreferenceConnector.readString(mContext,
                        PreferenceConnector.PHOTO_PATH3, "").equalsIgnoreCase("")) {
                    params.put("nama3", "photo3_" + count + ".jpg");
                    params.put("photo_3",
                            PreferenceConnector.readString(mContext,
                                    PreferenceConnector.PHOTO_PATH3, ""));
                }
                params.put("nama4", "sign_" + count + ".jpg");
                params.put("photo_4", sign);
                params.put("order_status", task.getOrder_status());
                return params;
            }
        };

        taskDataSource.close();
        // Adding request to request queue
        EkamantApplication.getInstance().addToRequestQueue(postRequest);
    }
}
