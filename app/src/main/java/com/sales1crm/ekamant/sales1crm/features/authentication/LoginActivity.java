/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.features.authentication;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sales1crm.ekamant.sales1crm.R;
import com.sales1crm.ekamant.sales1crm.features.shared.customview.CustomProgressDialog;
import com.sales1crm.ekamant.sales1crm.network.api.ApiParam;
import com.sales1crm.ekamant.sales1crm.network.api.EkamantApiService;
import com.sales1crm.ekamant.sales1crm.utils.Ekamant;
import com.sales1crm.ekamant.sales1crm.utils.PermissionCheck;
import com.sales1crm.ekamant.sales1crm.utils.PreferenceConnector;
import com.sales1crm.ekamant.sales1crm.utils.Utils;
import com.sales1crm.ekamant.sales1crm.utils.imageloader.ImageLoader;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * Created by Dea Synthia on 1/3/2017.
 */

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private String mAction = null;
    private String rawPasscode = "", passCode = "", reg_id = "";
    private int apiIndex;

    private EditText etPassword;
    private ImageView tvClear;
    private TextView tvLogin, tvWrongLogin;

    private Context mContext;
    private Intent mIntent = null;

    private HashMap<String, String> parameters;

    private CustomProgressDialog mLoading;

    private Messenger mService = null;
    private Messenger mMessenger = new Messenger(new LoginHandler(this));


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_login);
        this.mContext = this;
        Ekamant.context = this;
        Utils.imageLoader = new ImageLoader(this);

        //  Check Permission
        PermissionCheck permissionCheck = new PermissionCheck();
        permissionCheck.checkPermission(LoginActivity.this);

        if (Ekamant.iService == null) {
            Ekamant.iService = new Intent(LoginActivity.this,
                    EkamantApiService.class);
            startService(Ekamant.iService);
            Ekamant.directory = "Android/data/"
                    + getApplicationContext().getPackageName() + "/";
        }

        mLoading = CustomProgressDialog.createDialog(this, "", "");
        mLoading.setCancelable(false);

        initView();

        initializeBindService();
        getDataFromLoginUrl();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mIntent = getIntent();
        mAction = mIntent.getAction();
        Log.i(TAG, "onrestart : " + mIntent);
        Log.i(TAG, "url onrestart : " + mIntent.getData());
        if (Intent.ACTION_VIEW.equals(mAction)) {
            String toDecode = mIntent.getData().getEncodedQuery();
            String[] split = toDecode.split("=");
            toDecode = split[1] + "=";
            PreferenceConnector.writeString(mContext,
                    PreferenceConnector.ENCODED_PASSCODE, toDecode);
            Log.i(TAG, "todecode : " + toDecode);
            getDataFromLoginUrl();
        } else {
            Toast.makeText(this, "Wrong Passcode!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        mContext.unbindService(serviceConnection);
        mContext.stopService(Ekamant.iService);
        super.onDestroy();
    }

    private void initView() {
        etPassword = (EditText) findViewById(R.id.etPassword);
        Typeface myFont = Typeface.createFromAsset(getAssets(),
                "OpenSans-Light.ttf");
        etPassword.setTypeface(myFont);
        etPassword.setTransformationMethod(new PasswordTransformationMethod());
        etPassword.setEnabled(true);
        etPassword.setFocusable(true);
        etPassword.setClickable(true);
        etPassword.setFocusableInTouchMode(true);
        tvLogin = (TextView) findViewById(R.id.tvLogin);
        tvWrongLogin = (TextView) findViewById(R.id.tvWrongLogin);
        tvClear = (ImageView) findViewById(R.id.tvClear);
        tvLogin.setOnClickListener(click);
        tvClear.setOnClickListener(click);
    }

    private void initializeBindService() {
        if (mService == null) {
            bindService(Ekamant.iService, serviceConnection,
                    Context.BIND_AUTO_CREATE);
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder binder) {
            mService = new Messenger(binder);
            // doAPI(apiIndex);
        }
    };

    private void doApi(int apiIndex) {
        try {
            Message msg = Message.obtain(null, apiIndex);
            Bundle bundle = new Bundle();
            bundle.putSerializable("parameters", parameters);
            msg.setData(bundle);
            msg.replyTo = mMessenger;
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void sendDataLogin() {
        parameters = new HashMap<String, String>();
        parameters.put("passcode", passCode);
        //parameters.put("device_token", pref.getString("regId", null));
        parameters.put("device_token", PreferenceConnector.readString(mContext,
                PreferenceConnector.PREFERENCES_PROPERTY_REG_ID, ""));
        parameters.put("device_type", "2");
        apiIndex = ApiParam.API_001;

        doApi(apiIndex);
    }

    private static class LoginHandler extends Handler {
        WeakReference<LoginActivity> screen;

        public LoginHandler(LoginActivity myScreen) {
            screen = new WeakReference<LoginActivity>(myScreen);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handleMessage: " + msg.toString());

            switch (msg.what) {
                case ApiParam.API_001:
                    screen.get().getResponseLogin(msg);
                    break;
                default:
                    break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void getResponseLogin(Message msg) {
        HashMap<Object, Object> response =
                (HashMap<Object, Object>) msg.getData().getSerializable("response");

        Integer result = (Integer) response.get("result");
        switch (result) {
            case ApiParam.API_OK:
                doLogin(response);
                Log.i(TAG, "result: " + response);
                break;
            case ApiParam.API_NG:
                mLoading.dismiss();
                Toast.makeText(mContext, R.string.error_login, Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    @SuppressWarnings("unchecked")
    private void doLogin(HashMap<Object, Object> data) {
        // get user data from json
        HashMap<Object, Object> user_data = (HashMap<Object, Object>) ((HashMap<Object, Object>) data
                .get("datas")).get("user");
        String api_key = user_data.get("api_key").toString();
        Log.i(TAG, "doLogin: api key " + api_key);
        String user_name = user_data.get("name").toString();
        Log.i(TAG, "doLogin: username " + user_name);
        int user_id = Integer.parseInt(user_data.get("user_id").toString());

        String start_time = user_data.get("working_time_start").toString();
        String end_time = user_data.get("working_time_end").toString();

        String[] array_start = start_time.split(":");
        String[] array_end = end_time.split(":");

        int type = Integer.parseInt(user_data.get("type").toString());
        Log.i(TAG, "TYPE STAFF OR DRIVER : " + type);
        //1 STAFF, 2 DRIVER
        PreferenceConnector.writeInt(mContext,
                PreferenceConnector.DRIVER_STAFF, type);

        int start_hour, start_minute, end_hour, end_minute;
        start_hour = Integer.parseInt(array_start[0]);
        start_minute = Integer.parseInt(array_start[1]);
        end_hour = Integer.parseInt(array_end[0]);
        end_minute = Integer.parseInt(array_end[1]);
        Log.i(TAG, "STAR : " + start_hour + "," + start_minute);
        Log.i(TAG, "END : " + end_hour + "," + end_minute);

        PreferenceConnector.writeInt(mContext,
                PreferenceConnector.START_HOUR, start_hour);
        PreferenceConnector.writeInt(mContext,
                PreferenceConnector.START_MINUTE, start_minute);
        PreferenceConnector.writeInt(mContext, PreferenceConnector.END_HOUR,
                end_hour);
        PreferenceConnector.writeInt(mContext,
                PreferenceConnector.END_MINUTE, end_minute);

        PreferenceConnector.writeString(mContext, PreferenceConnector.API_KEY,
                api_key);
        PreferenceConnector.writeString(mContext, PreferenceConnector.USER_NAME,
                user_name);
        PreferenceConnector.writeInt(mContext, PreferenceConnector.USER_ID,
                user_id);
        Log.i(TAG,
                "user_id : "
                        + PreferenceConnector.readInt(mContext,
                        PreferenceConnector.USER_ID, -1));
        Log.i(TAG,
                "user_namr : "
                        + PreferenceConnector.readString(mContext,
                        PreferenceConnector.USER_NAME, ""));
        Log.i(TAG, "user_namr : " + user_name);
        mLoading.dismiss();
        if (!api_key.equals("")) {
            Toast.makeText(mContext, "Login Success!", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(LoginActivity.this, LoginConfActivity.class);
            i.putExtra("name", user_data.get("name").toString());
            i.putExtra("images", user_data.get("images").toString());
            i.putExtra("position", user_data.get("position").toString());
            i.putExtra("company", user_data.get("company").toString());
            startActivity(i);
            finish();
        }
    }

    private void getDataFromLoginUrl() {

        mIntent = getIntent();
        mAction = mIntent.getAction();

        Log.i(TAG, "getDataFromLoginUrl : " + mAction + mIntent.getData());
        Log.i(TAG, "url getDataFromLoginUrl : " + mIntent.getData());
        if (Intent.ACTION_VIEW.equals(mAction)) {
            String newToDecode = null;
            String toDecode = mIntent.getData().getEncodedQuery();

            try {
                newToDecode = java.net.URLDecoder.decode(toDecode, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String decodeResult = newToDecode.substring(2, newToDecode.length());
            Log.i(TAG, "getDataFromLoginUrl: decodeResult " + decodeResult);
            PreferenceConnector.writeString(mContext,
                    PreferenceConnector.ENCODED_PASSCODE, decodeResult);
            Log.i(TAG, "getDataFromLoginUrl todecode : " + newToDecode);
            passCodeCheck();
        } else {
            passCodeCheck();
        }
    }

    private void passCodeCheck() {
        String encodedPasscode = PreferenceConnector.readString(mContext,
                PreferenceConnector.ENCODED_PASSCODE, "");
        etPassword.setText(encodedPasscode);
    }


    View.OnClickListener click = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            switch (v.getId()) {
                case R.id.tvLogin:
                    if (etPassword.getText().toString().trim().length() > 0) {
                        tvWrongLogin.setVisibility(View.GONE);
                        mLoading.show();
                        // DECODE the password entered on the field
                        String toDecode = etPassword.getText().toString();
                        byte[] data = Base64.decode(toDecode, Base64.NO_PADDING);
                        try {
                            rawPasscode = new String(data, "UTF-8");
                            Log.i(TAG, "decoded text : " + rawPasscode);
                        } catch (UnsupportedEncodingException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        if (rawPasscode.contains("http://")) {
                            String[] ProcessingPasscode = rawPasscode.split("/");
                            for (int i = 0; i < ProcessingPasscode.length; i++) {
                                Log.i(TAG, "split : " + ProcessingPasscode[i]);
                            }
                            String url = "http://"
                                    + ProcessingPasscode[2].toString();
                            Log.i(TAG, "url : " + url);
                            PreferenceConnector.writeString(mContext,
                                    PreferenceConnector.URL, url);
                            passCode = ProcessingPasscode[3].substring(1);
                            Log.i(TAG, "passCode : " + passCode);
                            PreferenceConnector.writeString(mContext,
                                    PreferenceConnector.PASSCODE, passCode);

                            sendDataLogin();
                        } else {
                            Toast.makeText(mContext, "Invalid passcode",
                                    Toast.LENGTH_SHORT).show();
                            mLoading.dismiss();
                        }
                    } else {
                        Toast.makeText(mContext, "Input your passcode",
                                Toast.LENGTH_SHORT).show();
                        tvWrongLogin.setText("EMPTY PASSCODE");
                        tvWrongLogin.setVisibility(View.VISIBLE);
                    }
                    // Intent i = new Intent(LoginActivity.this,
                    // LoginConfirmationActivity.class);
                    // startActivity(i);
                    // finish();
                    break;
                case R.id.tvClear:
                    etPassword.setText("");
                    break;

                default:
                    break;
            }
        }
    };

}
