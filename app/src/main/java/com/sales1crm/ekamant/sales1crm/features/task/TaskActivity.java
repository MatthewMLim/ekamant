package com.sales1crm.ekamant.sales1crm.features.task;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.sales1crm.ekamant.sales1crm.R;
import com.sales1crm.ekamant.sales1crm.database.task.Task;
import com.sales1crm.ekamant.sales1crm.database.task.TaskDataSource;
import com.sales1crm.ekamant.sales1crm.features.authentication.CheckInActivity;
import com.sales1crm.ekamant.sales1crm.features.shared.TaskCustomAdapter;
import com.sales1crm.ekamant.sales1crm.features.shared.customview.CustomProgressDialog;
import com.sales1crm.ekamant.sales1crm.network.api.ApiParam;
import com.sales1crm.ekamant.sales1crm.network.api.EkamantApiService;
import com.sales1crm.ekamant.sales1crm.utils.Ekamant;
import com.sales1crm.ekamant.sales1crm.utils.PreferenceConnector;
import com.sales1crm.ekamant.sales1crm.utils.Utils;
import com.sales1crm.ekamant.sales1crm.utils.imageloader.ImageLoader;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Dea Synthia on 2/7/2017.
 */

public class TaskActivity extends AppCompatActivity {

    private static final String TAG = TaskActivity.class.getSimpleName();

    private String apiKey;
    private int apiIndex;

    private List<Task> orderList = new ArrayList<>();
    private HashMap<String, String> parameters;

    private ImageView backView, addOrderView;
    private ListView listView;

    private Context mContext;

    private CustomProgressDialog mLoading;
    private TaskCustomAdapter adapter;

    private Messenger mService = null;
    private Messenger mMessenger = new Messenger(new Settinghandler(this));

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_task);

        this.mContext = this;
        Ekamant.context = this;

        if (Utils.imageLoader == null) {
            Utils.imageLoader = new ImageLoader(this);
        }
        if (Ekamant.iService == null) {
            Ekamant.iService = new Intent(TaskActivity.this,
                    EkamantApiService.class);
            startService(Ekamant.iService);
            Ekamant.directory = "Android/data/"
                    + getApplicationContext().getPackageName() + "/";
        }

        mLoading = CustomProgressDialog.createDialog(this, "", "");
        mLoading.setCancelable(false);

        listView = (ListView) findViewById(R.id.list_order);
        TaskDataSource orderDb = new TaskDataSource(mContext);
        orderDb.open();
        orderList = orderDb.getAllTask();
        orderDb.close();
        adapter = new TaskCustomAdapter(this, orderList);
        listView.setAdapter(adapter);


        adapter.notifyDataSetChanged();

        initializeBindService();
        initView();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.i("AAA", "masuk new intent");
        // MUNGKIN CARA BUAT NGEHADAPIN MASALAH HOME BUTTON.. SKIP SEMENTARA
        super.onNewIntent(intent);
    }

    @Override
    protected void onRestart() {
        //if (!API_LONG_REQUEST) {
        //checkNotif();
        //doApi(apiIndex);
        //}
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        mContext.unbindService(serviceConnection);
        mContext.stopService(Ekamant.iService);
        super.onDestroy();
    }


    private void initializeBindService() {
        if (mService == null) {
            bindService(Ekamant.iService, serviceConnection,
                    Context.BIND_AUTO_CREATE);
        }
    }

    private void doLogout() {
        mLoading.show();
        parameters = new HashMap<String, String>();
        apiKey = PreferenceConnector.readString(mContext,
                PreferenceConnector.API_KEY, null);
        Log.i("TAG", "api key : " + apiKey);
        parameters.put("api_key", apiKey);
        apiIndex = ApiParam.API_005;
        doApi(apiIndex);
    }

    private void doApi(int apiIndex) {
        try {
            Message msg = Message.obtain(null, apiIndex);
            Bundle bundle = new Bundle();
            bundle.putSerializable("parameters", parameters);
            msg.setData(bundle);
            msg.replyTo = mMessenger;
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder binder) {
            mService = new Messenger(binder);
            //doApi(apiIndex);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }
    };


    private void initView() {
        addOrderView = (ImageView) findViewById(R.id.image_add);
        backView =(ImageView) findViewById(R.id.image_back);

        addOrderView.setOnClickListener(clickListener);
        backView.setOnClickListener(clickListener);


    }

    private static class Settinghandler extends Handler {
        WeakReference<TaskActivity> screen;

        public Settinghandler(TaskActivity myScreen) {
            screen = new WeakReference<TaskActivity>(myScreen);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.i("TAG", msg.toString());
            switch (msg.what) {
                /*case ApiParam.API_005:
                    screen.get().getResponseCheckout(msg);
                    break;*/
                default:
                    break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void getResponseCheckout(Message msg) {
        mLoading.dismiss();
        HashMap<Object, Object> response = (HashMap<Object, Object>) msg
                .getData().getSerializable("response");
        Log.i("TAG", "result : " + response);
        Integer result = (Integer) response.get("result");
        switch (result) {
            case ApiParam.API_OK:
                //API_LONG_REQUEST = false;
                //stopService(new Intent(context, LocationSendService.class));
                Intent i = new Intent(mContext, CheckInActivity.class);
                i.putExtra("is_checkout", true);
                startActivity(i);
                finish();
                break;
            case ApiParam.API_NG:
                Toast.makeText(mContext, "failed to checkout...", Toast.LENGTH_SHORT)
                        .show();
                //API_LONG_REQUEST = false;
                break;
            default:
                break;
        }
    }


    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_add:
                    Intent addOrder = new Intent(TaskActivity.this, TaskAddActivity.class);
                    startActivity(addOrder);
                    finish();
                    break;

                case R.id.image_back:
                    finish();
                    break;

                default:
                    break;
            }
        }
    };

}
