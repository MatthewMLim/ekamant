/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.network.api;

import android.util.Log;

import com.sales1crm.ekamant.sales1crm.utils.Ekamant;
import com.sales1crm.ekamant.sales1crm.utils.PreferenceConnector;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dea Synthia on 1/5/2017.
 */

public class ApiParam {

    private final static String TAG = ApiParam.class.getSimpleName();

    public final static int API_OK = 1;
    public final static int API_NG = 2;

    /**
     * Login
     */
    public final static int API_001 = 1;
    /**
     * Logout
     */
    public final static int API_002 = 2;
    /**
     * Check for checkin
     */
    public final static int API_003 = 3;
    /**
     * Check in
     */
    public final static int API_004 = 4;
    /**
     * Check out
     */
    public final static int API_005 = 5;
    /**
     * Tasklist
     */
    public final static int API_033 = 33;
    /**
     * Tasklist
     */
    public final static int API_034 = 34;
    /**
     * Tasklist
     */
    public final static int API_035 = 35;
    /**
     * Location send
     */
    public final static int API_045 = 45;




    public static String urlBuilder(int apiIndex, HashMap<String, String> parameters) {
        String url = PreferenceConnector.readString(Ekamant.context,
                PreferenceConnector.URL, "");
        url += getApiUrl(apiIndex);
        if (parameters == null) {
            return url;
        }

        Log.d(TAG, "Parameters API Param: " + parameters.toString());
        int i = 0;
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Log.d(TAG, "entry: " + entry.toString());
            Log.d(TAG, "key length: " + key.length());
            Log.d(TAG, "value: " + value.toString());
            int size = key.length() + value.length() + 2;
            StringBuilder builderParameter = new StringBuilder(size);
            if (i == 0) {
                url += builderParameter.append("?").append(key).append("=").append(value).toString();
            } else {
                url += builderParameter.append("&").append(key).append("=").append(value).toString();
            }
            i++;
        }
        Log.d(TAG, "urlBuilder: " + url);
        return url;
    }


    public static String getApiUrl(int apiIndex) {
        String apiUrl = "";
        switch (apiIndex) {
            case API_001:
                apiUrl = "/api/user/login";
                break;
            case API_002:
                apiUrl = "/api/user/logout";
                break;
            case API_003:
                apiUrl = "/api/user/get-status-checkin";
                break;
            case API_004:
                apiUrl = "/api/user/checkin";
                break;
            case API_005:
                apiUrl = "/api/user/checkout";
                break;
            case API_033:
                apiUrl = "/api/task/list";
                break;
            case API_034:
                apiUrl = "/api/task/finish";
                break;
            case API_035:
                apiUrl = "/api/crm-task/test";
                break;
            case API_045:
                apiUrl = "/api/user/location";
                break;
        }
        return apiUrl;
    }
}
