/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.features;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.sales1crm.ekamant.sales1crm.R;
import com.sales1crm.ekamant.sales1crm.features.authentication.CheckInActivity;
import com.sales1crm.ekamant.sales1crm.features.task.TaskActivity;
import com.sales1crm.ekamant.sales1crm.features.shared.customview.CustomDialog;
import com.sales1crm.ekamant.sales1crm.features.shared.customview.CustomProgressDialog;
import com.sales1crm.ekamant.sales1crm.network.api.ApiParam;
import com.sales1crm.ekamant.sales1crm.network.api.EkamantApiService;
import com.sales1crm.ekamant.sales1crm.utils.Ekamant;
import com.sales1crm.ekamant.sales1crm.utils.PreferenceConnector;
import com.sales1crm.ekamant.sales1crm.utils.Utils;
import com.sales1crm.ekamant.sales1crm.utils.imageloader.ImageLoader;

import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * TODO: Add a class Header comment!
 */

public class DashboardActivity extends AppCompatActivity {

    private static final String TAG = DashboardActivity.class.getSimpleName();

    private String apiKey;
    private int apiIndex;

    private HashMap<String, String> parameters;

    private ImageView logoutView, orderView;

    private Context mContext;

    private CustomProgressDialog mLoading;

    private Messenger mService = null;
    private Messenger mMessenger = new Messenger(new Settinghandler(this));

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_dashboard);

        this.mContext = this;
        Ekamant.context = this;

        if (Utils.imageLoader == null) {
            Utils.imageLoader = new ImageLoader(this);
        }
        if (Ekamant.iService == null) {
            Ekamant.iService = new Intent(DashboardActivity.this,
                    EkamantApiService.class);
            startService(Ekamant.iService);
            Ekamant.directory = "Android/data/"
                    + getApplicationContext().getPackageName() + "/";
        }

        mLoading = CustomProgressDialog.createDialog(this, "", "");
        mLoading.setCancelable(false);

        initializeBindService();
        initView();

    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.i("AAA", "masuk new intent");
        // MUNGKIN CARA BUAT NGEHADAPIN MASALAH HOME BUTTON.. SKIP SEMENTARA
        super.onNewIntent(intent);
    }

    @Override
    protected void onRestart() {
        //if (!API_LONG_REQUEST) {
        //checkNotif();
        doApi(apiIndex);
        //}
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        mContext.unbindService(serviceConnection);
        mContext.stopService(Ekamant.iService);
        super.onDestroy();
    }


    private void initializeBindService() {
        if (mService == null) {
            bindService(Ekamant.iService, serviceConnection,
                    Context.BIND_AUTO_CREATE);
        }
    }

    private void doLogout() {
        mLoading.show();
        parameters = new HashMap<String, String>();
        apiKey = PreferenceConnector.readString(mContext,
                PreferenceConnector.API_KEY, null);
        Log.i("TAG", "api key : " + apiKey);
        parameters.put("api_key", apiKey);
        apiIndex = ApiParam.API_005;
        doApi(apiIndex);
    }

    private void doApi(int apiIndex) {
        try {
            Message msg = Message.obtain(null, apiIndex);
            Bundle bundle = new Bundle();
            bundle.putSerializable("parameters", parameters);
            msg.setData(bundle);
            msg.replyTo = mMessenger;
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder binder) {
            mService = new Messenger(binder);
            //doApi(apiIndex);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }
    };


    private void initView() {
        logoutView = (ImageView) findViewById(R.id.ivLogout);
        orderView =(ImageView) findViewById(R.id.image_order); 

        logoutView.setOnClickListener(clickListener);
        orderView.setOnClickListener(clickListener);
    }

    private static class Settinghandler extends Handler {
        WeakReference<DashboardActivity> screen;

        public Settinghandler(DashboardActivity myScreen) {
            screen = new WeakReference<DashboardActivity>(myScreen);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.i("TAG", msg.toString());
            switch (msg.what) {
                case ApiParam.API_005:
                    screen.get().getResponseCheckout(msg);
                    break;
                default:
                    break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void getResponseCheckout(Message msg) {
        mLoading.dismiss();
        HashMap<Object, Object> response = (HashMap<Object, Object>) msg
                .getData().getSerializable("response");
        Log.i("TAG", "result : " + response);
        Integer result = (Integer) response.get("result");
        switch (result) {
            case ApiParam.API_OK:
                //API_LONG_REQUEST = false;
                //stopService(new Intent(context, LocationSendService.class));
                Intent i = new Intent(mContext, CheckInActivity.class);
                i.putExtra("is_checkout", true);
                startActivity(i);
                finish();
                break;
            case ApiParam.API_NG:
                Toast.makeText(mContext, "failed to checkout...", Toast.LENGTH_SHORT)
                        .show();
                //API_LONG_REQUEST = false;
                break;
            default:
                break;
        }
    }


    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.image_order:
                    startActivity( new Intent(DashboardActivity.this, TaskActivity.class));
                    break;

                case R.id.ivLogout:
                    Log.i("TAG", "v logout: " + v.getId());
                    final CustomDialog dialog = CustomDialog.createNormalDialog(mContext,
                            "Konfirmasi", "Apakah Anda ingin logout?", "",
                            Ekamant.TYPE_YESNO);
                    LinearLayout llYesNO = (LinearLayout) dialog
                            .findViewById(R.id.llYesNO);
                    (llYesNO.findViewById(R.id.tvYes)).setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            Log.i("TAG", "v logout: " + v.getId());
                            if (Utils.isNetworkAvailable(mContext)) {
                                mLoading.show();
                                doLogout();
                                //doSendLocation();
                                //doOrderCreateorPaymeDeli();
                            }
                        }
                    });
                    (llYesNO.findViewById(R.id.tvNo)).setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                    break;

                default:
                    break;
            }
        }
    };

}
