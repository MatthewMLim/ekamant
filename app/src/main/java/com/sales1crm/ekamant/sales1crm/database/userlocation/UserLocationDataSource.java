/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.database.userlocation;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.sales1crm.ekamant.sales1crm.database.DatabaseHelper;

/**
 * TODO: Add a class Header comment!
 */

public class UserLocationDataSource {

    private static final String TAG = UserLocationDataSource.class.getSimpleName();

    private SQLiteDatabase mDatabase;
    private DatabaseHelper dbHelper;

    public UserLocationDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() throws SQLException {
        mDatabase = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public long insertLocation(String latitude, String longitude, String input_date) {
        ContentValues values = new ContentValues();
        // values.put("id", id);
        values.put(UserLocationDb.KEY_LAT, latitude);
        values.put(UserLocationDb.KEY_LONG, longitude);
        values.put(UserLocationDb.KEY_INPUT_DATE, input_date);

        return mDatabase.insert(UserLocationDb.TABLE_NAME, null, values);
    }

    public void check_value_of_location() {
        String sql = "SELECT * FROM " + UserLocationDb.TABLE_NAME;
        Cursor mCur = mDatabase.rawQuery(sql, null);
        if (mCur != null && mCur.getCount() > 0) {
            mCur.moveToFirst();
            do {
                Log.i("HUAI",
                        "location id: "
                                + mCur.getInt(mCur.getColumnIndex(UserLocationDb.KEY_ROW_ID)));
                Log.i("HUAI",
                        " location latitude: "
                                + mCur.getString(mCur
                                .getColumnIndex(UserLocationDb.KEY_LAT)));
                Log.i("HUAI",
                        "location longitude: "
                                + mCur.getString(mCur
                                .getColumnIndex(UserLocationDb.KEY_LONG)));
                Log.i("HUAI",
                        "location input_date: "
                                + mCur.getString(mCur
                                .getColumnIndex(UserLocationDb.KEY_INPUT_DATE)));

            } while (mCur.moveToNext());

        }
        // String id = mCur.getString(mCur.getColumnIndex("id"));

        mCur.close();
    }

    public Cursor getCursor_location() {
        try {
            String sql = "SELECT * FROM " + UserLocationDb.TABLE_NAME + " ORDER BY " +
                    UserLocationDb.KEY_ROW_ID;
            Log.i("AAA", "sql : " + sql);
            Cursor mCur = mDatabase.rawQuery(sql, null);
            return mCur;
        } catch (SQLException mSQLException) {
            Log.i("DB", "getCursor_location >>" + mSQLException.toString());
            throw mSQLException;
        }
    }
}
