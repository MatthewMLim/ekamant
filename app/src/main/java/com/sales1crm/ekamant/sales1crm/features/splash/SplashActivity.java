/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.features.splash;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.StrictMode;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.sales1crm.ekamant.sales1crm.R;
import com.sales1crm.ekamant.sales1crm.features.DashboardActivity;
import com.sales1crm.ekamant.sales1crm.features.authentication.CheckInActivity;
import com.sales1crm.ekamant.sales1crm.features.authentication.LoginActivity;
import com.sales1crm.ekamant.sales1crm.network.api.ApiParam;
import com.sales1crm.ekamant.sales1crm.network.api.EkamantApiService;
import com.sales1crm.ekamant.sales1crm.network.firebase.FirebaseNotification;
import com.sales1crm.ekamant.sales1crm.utils.Ekamant;
import com.sales1crm.ekamant.sales1crm.utils.PreferenceConnector;
import com.sales1crm.ekamant.sales1crm.utils.Utils;
import com.sales1crm.ekamant.sales1crm.utils.imageloader.ImageLoader;

import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * TODO: Add a class Header comment!
 */

public class SplashActivity extends AppCompatActivity {

    private final static String TAG = SplashActivity.class.getSimpleName();
    private static int SPLASH_TIME_OUT = 2000;

    private String apiKey = "";
    private String mAction = null;
    private int checkIn = 1;
    private int apiIndex;

    private HashMap<String, String> parameters;

    private Context mContext;
    private Intent mIntent;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private Messenger mService = null;
    private Messenger mMessenger = new Messenger(new CheckInCheckHandler(this));


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_splash_screen);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        mContext = this;
        Ekamant.context = this;
        Log.i(TAG, "Utils.imageLoader " + Utils.imageLoader);
        Utils.imageLoader = new ImageLoader(this);

        Log.i(TAG, "service : " + Ekamant.iService);
        Ekamant.iService = new Intent(SplashActivity.this, EkamantApiService.class);
        startService(Ekamant.iService);
        Ekamant.directory = "Android/data/"
                + getApplicationContext().getPackageName() + "/";

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Ekamant.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Ekamant.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Ekamant.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };

        runUiThread();
        getDataFromURL();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Ekamant.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Ekamant.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        FirebaseNotification.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onRestart() {
        // TODO Auto-generated method stub
        super.onRestart();
        Log.i(TAG, "onrestart");

        mIntent = getIntent();
        mAction = mIntent.getAction();
        Log.i(TAG, "onrestart : " + mAction + mIntent);
        if (Intent.ACTION_VIEW.equals(mAction)) {
            String toDecode = mIntent.getData().getEncodedQuery();
            String[] split = toDecode.split("=");
            toDecode = split[1];
            PreferenceConnector.writeString(mContext,
                    PreferenceConnector.ENCODED_PASSCODE, toDecode);
            Log.i(TAG, "todecode : " + toDecode);
        }
    }

    @Override
    protected void onDestroy() {
        try {
            mContext.unbindService(serviceConnection);
            mContext.stopService(Ekamant.iService);
            Log.i(TAG, "mservice destroyed : " + mService);
        } catch (Exception e) {
            // TODO: handle exception
        }
        super.onDestroy();
    }

    private void initializeBindService() {
        parameters = new HashMap<String, String>();
        parameters.put("api_key", PreferenceConnector.readString(mContext,
                PreferenceConnector.API_KEY, ""));
        apiIndex = ApiParam.API_003;
        if (mService == null) {
            Log.i(TAG, "mservice start : " + mService);
            bindService(Ekamant.iService, serviceConnection,
                    Context.BIND_AUTO_CREATE);
        }
    }

    private void getDataFromURL() {
        mIntent = getIntent();
        mAction = mIntent.getAction();
        Log.i(TAG, "onrestart : " + mAction + mIntent);
        if (Intent.ACTION_VIEW.equals(mAction)) {
            String toDecode = mIntent.getData().getEncodedQuery();
            String[] split = toDecode.split("=");
            toDecode = split[1];
            PreferenceConnector.writeString(mContext,
                    PreferenceConnector.ENCODED_PASSCODE, toDecode);
            Log.i(TAG, "todecode : " + toDecode);
        }
    }

    // For changing the first intent from link if the page already opened first
    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub
        super.onNewIntent(intent);
        setIntent(intent);
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder binder) {
            mService = new Messenger(binder);
            doApi(apiIndex);
            Log.i(TAG, "mservice started : " + mService);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }

    };

    private void doApi(int apiIndex) {
        try {
            Message msg = Message.obtain(null, apiIndex);
            Bundle bundle = new Bundle();
            bundle.putSerializable("parameters", parameters);
            msg.setData(bundle);
            msg.replyTo = mMessenger;
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private static class CheckInCheckHandler extends Handler {
        WeakReference<SplashActivity> screen;

        public CheckInCheckHandler(SplashActivity myScreen) {
            screen = new WeakReference<SplashActivity>(myScreen);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "Message : " + msg.toString());
            switch (msg.what) {
                case ApiParam.API_003:
                    screen.get().getCheckInCheck(msg);
                    break;
                default:
                    break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void getCheckInCheck(Message msg) {
        HashMap<Object, Object> response = (HashMap<Object, Object>) msg
                .getData().getSerializable("response");
        Log.i(TAG, "result : " + response);
        Integer result = (Integer) response.get("result");
        switch (result) {
            case ApiParam.API_OK:
                HashMap<Object, Object> user_data = (HashMap<Object, Object>) response
                        .get("datas");
                Log.i(TAG, "status : " + user_data.get("status"));

                PreferenceConnector.writeInt(mContext,
                        PreferenceConnector.CHECKIN,
                        Integer.parseInt(user_data.get("status").toString()));

                if (PreferenceConnector.readString(mContext,
                        PreferenceConnector.URL, "").contains("ekamant")) {
                    if (user_data.get("user_id") != null) {
                        PreferenceConnector.writeString(mContext,
                                PreferenceConnector.USER_ID,
                                user_data.get("user_id").toString());
                    }
                }

                apiKey = PreferenceConnector.readString(mContext,
                        PreferenceConnector.API_KEY, "");
                Log.i(TAG, "api_key : " + apiKey);

                checkIn = PreferenceConnector.readInt(mContext,
                        PreferenceConnector.CHECKIN,
                        Ekamant.DEFAULT_VALUEFORCHECK_IN);

                Log.i(TAG, "check in : " + checkIn);

                if (apiKey.equals("")) {
                    // no api_key so go to tutorial first
                    Intent i = new Intent(SplashActivity.this,
                            LoginActivity.class);
                    startActivity(i);
                } else if (checkIn == 1) {
                    // there is api_key, but not check in yet, go to check in, skip
                    // tutor and login
                    Intent i = new Intent(SplashActivity.this,
                            CheckInActivity.class);
                    startActivity(i);
                } else if (checkIn == 2) {
                    // already check in, have apikey so get to work, go to dashboard
                    Intent i = new Intent(SplashActivity.this,
                            DashboardActivity.class);
                    startActivity(i);
                } else if (checkIn == 0) {
                    Intent i = new Intent(SplashActivity.this,
                            CheckInActivity.class);
                    i.putExtra("is_checkout", true);
                    startActivity(i);
                }
                // close this activity
                finish();
                break;
            case ApiParam.API_NG:
                // Toast.makeText(context, "Login Failed Check your password",
                // Toast.LENGTH_SHORT).show();
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
                break;
            default:
                break;
        }
    }

    private void runUiThread() {
        new Handler().postDelayed(new Runnable() {
            //  This method will be execute once the timer is over
            @Override
            public void run() {

                if (PreferenceConnector.readString(mContext,
                        PreferenceConnector.API_KEY, "").length() > 0) {
                    initializeBindService();
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    //  Close this activity
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }


    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        Log.e(TAG, "Firebase reg id: "
                + PreferenceConnector.readString(
                mContext, PreferenceConnector.PREFERENCES_DEVICE_TOKEN, ""));
    }

}
