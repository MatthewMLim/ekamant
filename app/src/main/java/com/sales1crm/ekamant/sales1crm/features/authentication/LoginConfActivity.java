/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.features.authentication;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sales1crm.ekamant.sales1crm.R;
import com.sales1crm.ekamant.sales1crm.features.DashboardActivity;
import com.sales1crm.ekamant.sales1crm.features.shared.customview.CustomDialog;
import com.sales1crm.ekamant.sales1crm.features.shared.customview.CustomProgressDialog;
import com.sales1crm.ekamant.sales1crm.network.api.ApiParam;
import com.sales1crm.ekamant.sales1crm.utils.Ekamant;
import com.sales1crm.ekamant.sales1crm.utils.PreferenceConnector;
import com.sales1crm.ekamant.sales1crm.utils.Utils;

import java.lang.ref.WeakReference;
import java.util.HashMap;

/**
 * TODO: Add a class Header comment!
 */

public class LoginConfActivity extends AppCompatActivity {

    public static final String TAG = LoginConfActivity.class.getSimpleName();

    private String name, images, position, company, url;
    private String apiKey = "";
    private int apiIndex;
    private int checkIn = 1;
    private boolean hasService = false;

    private HashMap<String, String> parameters;

    private TextView nameView, companyView, fullNameView, positionView;
    private ImageView profPictView;
    private Button okView, noView;

    private Context mContext;

    private CustomProgressDialog mLoading;
    private CustomDialog mDialog;

    private Messenger mService = null;
    private Messenger mMessenger = new Messenger(new CheckInCheckHandler2(this));

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_login_conf);

        mContext = this;
        Ekamant.context = this;

        url = PreferenceConnector.readString(mContext, PreferenceConnector.URL, "");
        PreferenceConnector.writeBoolean(mContext,PreferenceConnector.WORK_START_NOTIF, false);
        PreferenceConnector.writeBoolean(mContext,PreferenceConnector.WORK_END_NOTIF, false);
        PreferenceConnector.writeBoolean(mContext, PreferenceConnector.REMINDME,false);

        try {
            name = getIntent().getExtras().getString("name");
            images = getIntent().getExtras().getString("images");
            position = getIntent().getExtras().getString("position");
            company = getIntent().getExtras().getString("company");
        } catch (Exception e) {
            // TODO: handle exception
        }

        mLoading = CustomProgressDialog.createDialog(this, "", "");
        mLoading.setCancelable(false);

        initView();

    }

    @Override
    protected void onDestroy() {
        if (hasService) {
            mContext.unbindService(serviceConnection);
            mContext.stopService(Ekamant.iService);
        }
        super.onDestroy();
    }

    private void initializeBindService() {
        mLoading.show();
        parameters = new HashMap<String, String>();
        parameters.put("api_key", PreferenceConnector.readString(mContext,
                PreferenceConnector.API_KEY, ""));
        apiIndex = ApiParam.API_003;
        if (mService == null) {
            hasService = true;
            bindService(Ekamant.iService, serviceConnection,
                    Context.BIND_AUTO_CREATE);
        }
    }

    // findviewbyid for required field
    private void initView() {
        nameView = (TextView) findViewById(R.id.text_hello);
        nameView.setText("Hello " + (name.split(" "))[0] + "!");

        companyView = (TextView) findViewById(R.id.text_company_name);
        companyView.setText(company);

        fullNameView = (TextView) findViewById(R.id.text_full_name);
        fullNameView.setText(name);

        positionView = (TextView) findViewById(R.id.text_position_name);
        positionView.setText(position);

        profPictView = (ImageView) findViewById(R.id.image_prof_pict);
        if (images.length() != 0) {
            Utils.imageLoader.DisplayImage(url + images, profPictView,
                    Bitmap.Config.ARGB_8888, 150, Ekamant.TYPE_ROUND);
        }

        okView = (Button) findViewById(R.id.btn_ok);
        okView.setOnClickListener(clickListener);

        noView = (Button) findViewById(R.id.btn_no);
        noView.setOnClickListener(clickListener);
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder binder) {
            mService = new Messenger(binder);
            doAPI(apiIndex);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }
    };

    private void doAPI(int apiIndex) {
        try {
            Message msg = Message.obtain(null, apiIndex);
            Bundle bundle = new Bundle();
            bundle.putSerializable("parameters", parameters);
            msg.setData(bundle);
            msg.replyTo = mMessenger;
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private static class CheckInCheckHandler2 extends Handler {
        WeakReference<LoginConfActivity> screen;

        public CheckInCheckHandler2(LoginConfActivity myScreen) {
            screen = new WeakReference<LoginConfActivity>(myScreen);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.i("TAG", msg.toString());
            switch (msg.what) {
                case ApiParam.API_003:
                    screen.get().getCheckInCheck(msg);
                    break;
                default:
                    break;
            }

        }
    }

    @SuppressWarnings("unchecked")
    private void getCheckInCheck(Message msg) {
        HashMap<Object, Object> response = (HashMap<Object, Object>) msg
                .getData().getSerializable("response");
        Log.i("TAG", "result : " + response);
        Integer result = (Integer) response.get("result");

        switch (result) {
            case ApiParam.API_OK:
                HashMap<Object, Object> user_data = (HashMap<Object, Object>) response
                        .get("datas");
                Log.i("TAG", "status : " + user_data.get("status"));

                PreferenceConnector.writeInt(mContext,
                        PreferenceConnector.CHECKIN,
                        Integer.parseInt(user_data.get("status").toString()));

                /*if (PreferenceConnector.readString(mContext,
                        PreferenceConnector.URL, "").contains("tikindo")) {
                    if (user_data.get("user_id") != null) {
                        PreferenceConnector.writeString(mContext,
                                PreferenceConnector.USER_ID,
                                user_data.get("user_id").toString());
                    }
                }*/

                apiKey = PreferenceConnector.readString(mContext,
                        PreferenceConnector.API_KEY, "");
                Log.i("TAG", "api_key : " + apiKey);

                checkIn = PreferenceConnector.readInt(mContext,
                        PreferenceConnector.CHECKIN,
                        Ekamant.DEFAULT_VALUEFORCHECK_IN);
                Log.i("TAG", "check in : " + checkIn);
                mLoading.dismiss();

                if (checkIn == 1) {
                    // not check in yet, go to check in
                    Intent i = new Intent(LoginConfActivity.this,
                            CheckInActivity.class);
                    startActivity(i);
                } else if (checkIn == 2) {
                    // already check in so get to work, go to dashboard
                    Intent i = new Intent(LoginConfActivity.this,
                            DashboardActivity.class);
                    startActivity(i);
                } else if (checkIn == 0) {
                    Intent i = new Intent(LoginConfActivity.this,
                            CheckInActivity.class);
                    i.putExtra("is_checkout", true);
                    startActivity(i);
                }
                // close this activity
                finish();

                break;
            case ApiParam.API_NG:
                if (PreferenceConnector.readInt(mContext,
                        PreferenceConnector.CHECKIN,
                        Ekamant.DEFAULT_VALUEFORCHECK_IN) == 1
                        || PreferenceConnector.readInt(mContext,
                        PreferenceConnector.CHECKIN,
                        Ekamant.DEFAULT_VALUEFORCHECK_IN) == 0) {
                    Intent i = new Intent(LoginConfActivity.this,
                            CheckInActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(LoginConfActivity.this,
                            DashboardActivity.class);
                    startActivity(i);
                    finish();
                }
                mLoading.dismiss();
                break;
            default:
                break;
        }
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_ok:
                    initializeBindService();
                    break;

                case R.id.btn_no:
                    // TODO Auto-generated method stub
                    mDialog = CustomDialog.createNormalDialog(mContext,
                            "CONFIRMATION", "Are you sure you want to", "logout?",
                            Ekamant.TYPE_YESNO);

                    TextView tvTitle = (TextView) mDialog.findViewById(R.id.tvTitle);
                    TextView tvText1 = (TextView) mDialog.findViewById(R.id.tvText1);
                    TextView tvText2 = (TextView) mDialog.findViewById(R.id.tvText2);
                    LinearLayout llYesNO = (LinearLayout) mDialog
                            .findViewById(R.id.llYesNO);
                    LinearLayout llOK = (LinearLayout) mDialog
                            .findViewById(R.id.llOK);
                    TextView tvYes = (TextView) mDialog.findViewById(R.id.tvYes);
                    TextView tvNo = (TextView) mDialog.findViewById(R.id.tvNo);

                    tvTitle.setText("CONFIRMATION");
                    tvText1.setText("Are you sure you want to");
                    tvText2.setText("logout?");

                    llOK.setVisibility(View.GONE);
                    llYesNO.setVisibility(View.VISIBLE);

                    tvYes.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            mDialog.dismiss();
                            PreferenceConnector.writeString(mContext,
                                    PreferenceConnector.API_KEY, "");
                            Intent i = new Intent(LoginConfActivity.this,
                                    LoginActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });
                    tvNo.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                            mDialog.dismiss();
                        }
                    });
                    mDialog.show();
                    break;

                default:
                    break;
            }
        }
    };
}
