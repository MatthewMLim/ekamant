/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.database.task;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.sales1crm.ekamant.sales1crm.database.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Add a class Header comment!
 */

public class TaskDataSource {

    public static final String TAG = TaskDataSource.class.getSimpleName();

    private SQLiteDatabase mDatabase;
    private DatabaseHelper dbHelper;

    public TaskDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() throws SQLException {
        mDatabase = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public long countDb() {

        String selectQuery = "SELECT COUNT(*) FROM " + TaskDb.TABLE_NAME;

        SQLiteStatement sqLiteStatement = mDatabase.compileStatement(selectQuery);

        return sqLiteStatement.simpleQueryForLong();

    }

    public void insertTask(int customerId, String customerName, String notes, String order,
                           String latitude, String longitude, String created_at) {

        ContentValues values = new ContentValues();

        values.put(TaskDb.KEY_CUSTOMER_ID, customerId);
        values.put(TaskDb.KEY_CUSTOMER_NAME, customerName);
        values.put(TaskDb.KEY_NOTES, notes);
        values.put(TaskDb.KEY_ORDER_STATUS, order);
        values.put(TaskDb.KEY_LAT, latitude);
        values.put(TaskDb.KEY_LONG, longitude);
        values.put(TaskDb.KEY_CREATED_AT, created_at);

        mDatabase.insert(TaskDb.TABLE_NAME, null, values);
    }

    public void insertImage(int customerID, String column, String path) {

    }

    public List<Task> getAllTask() {
        List<Task> list = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TaskDb.TABLE_NAME + " ORDER BY _id DESC";
        Cursor cursor = mDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                Task task = new Task();
                task.setId(Integer.valueOf(cursor.getString(0)));
                task.setCustomer_name(cursor.getString(2));
                task.setNotes(cursor.getString(3));
                task.setOrder_status(cursor.getString(4));
                task.setCreated_at(cursor.getString(7));

                list.add(task);
                cursor.moveToNext();
            }
        }

        return list;
    }

    public Task getSingleTask( String id){

        Task task = new Task();

        String selectQuery = "SELECT * FROM " + TaskDb.TABLE_NAME + " WHERE _id=" + id;
        Cursor cursor = mDatabase.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                task.setId(Integer.valueOf(cursor.getString(0)));
                task.setCustomer_id(Integer.parseInt(cursor.getString(1)));
                task.setNotes(cursor.getString(3));
                task.setOrder_status(cursor.getString(4));
                task.setLatitude(cursor.getString(5));
                task.setLongitude(cursor.getString(6));
                task.setCreated_at(cursor.getString(7));

                cursor.moveToNext();
            }
        }

        return task;
    }


}
