/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.utils.imageloader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;


import com.sales1crm.ekamant.sales1crm.R;
import com.sales1crm.ekamant.sales1crm.utils.Ekamant;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class ImageLoader {

	MemoryCache memoryCache = new MemoryCache();
	FileCache fileCache;
	private Bitmap.Config imageConfig;
	private int reqSize;
	private Map<ImageView, String> imageViews = Collections
			.synchronizedMap(new WeakHashMap<ImageView, String>());
	ThreadPoolExecutor executorService;

	public ImageLoader(Context context) {
		fileCache = new FileCache(context);
		executorService = new ThreadPoolExecutor(5, 5, 1000,
				TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(5));
		executorService
				.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
	}

	final int stub_id = R.drawable.avatar_placeholder;
	final int stub_id_rect = R.drawable.account_avatar;
	private int type = Ekamant.TYPE_ROUND;

	public void DisplayImage(String url, ImageView imageView,
							 Bitmap.Config imageConfig, int reqSize, int type) {
		imageViews.put(imageView, url);
		this.type = type;
		WeakReference<Bitmap> bitmap = new WeakReference<Bitmap>(
				memoryCache.get(url));
		this.imageConfig = Config.ARGB_8888;
		this.reqSize = reqSize;
		if (bitmap.get() != null) {
			if (type == Ekamant.TYPE_ROUND) {
				imageView.setImageBitmap(com.sales1crm.ekamant.sales1crm.utils.Utils
						.getroundBitmap(bitmap.get()));
			} else {
				imageView.setImageBitmap(com.sales1crm.ekamant.sales1crm.utils.Utils
						.getrectBitmap(bitmap.get()));
			}
		} else {
			queuePhoto(url, imageView);
			if (type == Ekamant.TYPE_ROUND) {
				imageView.setImageResource(stub_id);
			} else {
				imageView.setImageResource(stub_id_rect);
			}
			// Log.d("TAG","URL not found, use default"); TODO
		}
	}

	private void queuePhoto(String url, ImageView imageView) {
		PhotoToLoad p = new PhotoToLoad(url, imageView);
		executorService.submit(new PhotosLoader(p));
		// Log.i(BaseID.tag,"queue poto!!!");
	}

	// public Bitmap getBitmap(String url) {
	// File f = fileCache.getFile(url);
	//
	// // from SD cache
	// Bitmap b = decodeFile(f);
	// if (b != null)
	// return b;
	//
	// // from web
	// try {
	// Bitmap bitmap = null;
	// URL imageUrl = new URL(url);
	// HttpURLConnection conn = (HttpURLConnection) imageUrl
	// .openConnection();
	// conn.setConnectTimeout(30000);
	// conn.setReadTimeout(30000);
	// conn.setInstanceFollowRedirects(true);
	// InputStream is = conn.getInputStream();
	// OutputStream os = new FileOutputStream(f);
	// Utils.CopyStream(is, os);
	// os.close();
	// bitmap = decodeFile(f);
	// return bitmap;
	// } catch (Exception ex) {
	// // ex.printStackTrace();
	// // Log.i("TAG","url error, bitmap not found"); TODO
	// return null;
	// }
	// }
	//
	// // decodes image and scales it to reduce memory consumption
	// private Bitmap decodeFile(File f) {
	// try {
	// // decode image size
	// BitmapFactory.Options o = new BitmapFactory.Options();
	// o.inJustDecodeBounds = true;
	// BitmapFactory.decodeStream(new FileInputStream(f), null, o);
	//
	// // Find the correct scale value. It should be the power of 2.
	// final int REQUIRED_SIZE = 40;
	// int width_tmp = o.outWidth, height_tmp = o.outHeight;
	// int scale = 1;
	// while (true) {
	// if (width_tmp / 2 < REQUIRED_SIZE
	// || height_tmp / 2 < REQUIRED_SIZE)
	// break;
	// width_tmp /= 2;
	// height_tmp /= 2;
	// scale *= 2;
	// }
	//
	// // decode with inSampleSize
	// BitmapFactory.Options o2 = new BitmapFactory.Options();
	// o2.inSampleSize = scale;
	// return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
	// } catch (FileNotFoundException e) {
	// }
	// return null;
	// }

	public Bitmap getBitmap2(String url) {
		File f = fileCache.getFile(url);

		// from SD cache
		Bitmap b = decodeFile2(f);
		if (b != null)
			return b;

		// from web
		try {
			Log.i("TAG", "URLNYA " + url);
			Bitmap bitmap = null;
			URL imageUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) imageUrl
					.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			InputStream is = conn.getInputStream();
			OutputStream os = new FileOutputStream(f);
			Utils.CopyStream(is, os);
			os.close();
			bitmap = decodeFile2(f);
			return bitmap;
		} catch (Exception ex) {
			// ex.printStackTrace();
			// Log.i("TAG", "url error, bitmap not found");
			return null;
		}
	}

	// decodes image and scales it to reduce memory consumption
	private Bitmap decodeFile2(File f) {
		try {
			// Decode image size
			int REQUIRED_SIZE = reqSize;
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			o.inPurgeable = true;
			o.inPreferredConfig = imageConfig;

			FileInputStream fis = new FileInputStream(f);
			BitmapFactory.decodeStream(fis, null, o);
			try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			int scale = 1;
			if (o.outHeight > REQUIRED_SIZE || o.outWidth > REQUIRED_SIZE) {
				scale = (int) Math.pow(
						2,
						(int) Math.round(Math.log(REQUIRED_SIZE
								/ (double) Math.max(o.outHeight, o.outWidth))
								/ Math.log(0.5)));
			}

			// decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			o2.inPurgeable = true;
			o2.inPreferredConfig = imageConfig;
			WeakReference<Bitmap> bitmap = new WeakReference<Bitmap>(
					BitmapFactory
							.decodeStream(new FileInputStream(f), null, o2));
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			bitmap.get().compress(Bitmap.CompressFormat.PNG, 100, out);
			bitmap.get().recycle();
			bitmap = null;
			WeakReference<Bitmap> bitmap2 = new WeakReference<Bitmap>(
					BitmapFactory.decodeByteArray(out.toByteArray(), 0,
							out.toByteArray().length));
			return bitmap2.get();
		} catch (FileNotFoundException e) {
		}
		return null;
	}

	// Task for the queue
	private class PhotoToLoad {
		public String url;
		public ImageView imageView;

		public PhotoToLoad(String u, ImageView i) {
			url = u;
			imageView = i;
		}
	}

	class PhotosLoader implements Runnable {
		PhotoToLoad photoToLoad;

		PhotosLoader(PhotoToLoad photoToLoad) {
			this.photoToLoad = photoToLoad;
		}

		@Override
		public void run() {
			if (imageViewReused(photoToLoad))
				return;
			Bitmap bmp = getBitmap2(photoToLoad.url);
			Log.i("ACRA", "bmp : " + bmp);
			memoryCache.put(photoToLoad.url, bmp);
			if (imageViewReused(photoToLoad))
				return;
			BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
			Activity a = (Activity) photoToLoad.imageView.getContext();
			a.runOnUiThread(bd);
		}
	}

	boolean imageViewReused(PhotoToLoad photoToLoad) {
		String tag = imageViews.get(photoToLoad.imageView);
		if (tag == null || !tag.equals(photoToLoad.url))
			return true;
		return false;
	}

	// Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable {
		Bitmap bitmap;
		PhotoToLoad photoToLoad;

		public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
			bitmap = b;
			photoToLoad = p;
		}

		public void run() {
			if (imageViewReused(photoToLoad))
				return;
			if (bitmap != null) {
				Log.i("ACRA", "bitmap not null, run");
				if (type == Ekamant.TYPE_ROUND) {
					photoToLoad.imageView.setImageBitmap(com.sales1crm.ekamant.sales1crm.utils.Utils
							.getroundBitmap(bitmap));
				} else {
					photoToLoad.imageView.setImageBitmap(com.sales1crm.ekamant.sales1crm.utils.Utils
							.getrectBitmap(bitmap));
				}

			} else {
				Log.i("ACRA", "bitmap null, run");
				if (type == Ekamant.TYPE_ROUND) {
					photoToLoad.imageView.setImageResource(stub_id);
				} else {
					photoToLoad.imageView.setImageResource(stub_id_rect);
				}
			}
		}
	}

	public void clearCache() {
		memoryCache.clear();
		fileCache.clear();
	}

}
