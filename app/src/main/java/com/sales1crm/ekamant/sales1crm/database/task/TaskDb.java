/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.database.task;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * TODO: Add a class Header comment!
 */

public class TaskDb {

    private static final String TAG = TaskDb.class.getSimpleName();

    public static final String KEY_ROW_ID = "_id"; // 0
    public static final String KEY_CUSTOMER_ID = "account_id"; //1
    public static final String KEY_CUSTOMER_NAME = "account_name"; // as subject - 2
    public static final String KEY_NOTES = "notes"; // 3
    public static final String KEY_ORDER_STATUS = "order_status"; // 4
    public static final String KEY_LAT = "latitude"; // 5
    public static final String KEY_LONG = "longitude"; // 6
    public static final String KEY_CREATED_AT = "created_at"; // 7

    public static final String TABLE_NAME = "task_tb";

    private static final String CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                    KEY_ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , " +
                    KEY_CUSTOMER_ID + ", " +
                    KEY_CUSTOMER_NAME + ", " +
                    KEY_NOTES + ", " +
                    KEY_ORDER_STATUS + ", " +
                    KEY_LAT + ", " +
                    KEY_LONG + ", " +
                    KEY_CREATED_AT + " )";

    public static void onCreate(SQLiteDatabase db) {
        Log.i(TAG, "onCreate: " + CREATE_TABLE);
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpdate(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "onUpdate: " + "Upgrate Database from " + oldVersion + " to " + newVersion);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL(CREATE_TABLE);
    }
}
