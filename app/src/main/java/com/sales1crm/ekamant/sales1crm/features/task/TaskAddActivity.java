/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.features.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.sales1crm.ekamant.sales1crm.R;
import com.sales1crm.ekamant.sales1crm.app.EkamantApplication;
import com.sales1crm.ekamant.sales1crm.database.task.TaskDataSource;
import com.sales1crm.ekamant.sales1crm.features.DashboardActivity;
import com.sales1crm.ekamant.sales1crm.features.shared.customview.CustomProgressDialog;
import com.sales1crm.ekamant.sales1crm.features.verification.SignatureActivity;
import com.sales1crm.ekamant.sales1crm.network.api.ApiParam;
import com.sales1crm.ekamant.sales1crm.network.api.EkamantApiService;
import com.sales1crm.ekamant.sales1crm.utils.Ekamant;
import com.sales1crm.ekamant.sales1crm.utils.GpsTracker;
import com.sales1crm.ekamant.sales1crm.utils.PreferenceConnector;
import com.sales1crm.ekamant.sales1crm.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Add a class Header comment!
 */

public class TaskAddActivity extends AppCompatActivity {

    private static final String TAG = TaskAddActivity.class.getSimpleName();

    String myBase64Image;
    private String customerName = "", notes = "",
            latitude, longitude,
            createdAt;
    private int orderStatus = 0;
    private int apiIndex;
    private int REQUEST_CAMERA = 0;

    private HashMap<String, String> parameters;

    private EditText accountNameView, notesView;
    private ImageView backView;
    private RadioGroup groupView;
    private Button submitView, takePictView1, takePictView2, takePictView3;

    private Context mContext;
    private Bundle bundle;

    private CustomProgressDialog mLoading;

    private Messenger mService = null;
    private Messenger mMessenger = new Messenger(new SubmitHandler(this));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_task_add);

        mContext = this;
        Ekamant.context = this;
        bundle = new Bundle();

        if (Ekamant.iService == null) {
            Ekamant.iService = new Intent(TaskAddActivity.this,
                    EkamantApiService.class);
            startService(Ekamant.iService);
            Ekamant.directory = "Android/data/"
                    + getApplicationContext().getPackageName() + "/";
        }

        mLoading = CustomProgressDialog.createDialog(this, "", "");
        mLoading.setCancelable(false);

        initializeBindService();

        initView();
        getLocation();
    }

    @Override
    protected void onDestroy() {
        mContext.unbindService(serviceConnection);
        mContext.stopService(Ekamant.iService);
        super.onDestroy();
    }

    public void initView() {
        accountNameView = (EditText) findViewById(R.id.edit_account_name);
        notesView = (EditText) findViewById(R.id.edit_notes);
        groupView = (RadioGroup) findViewById(R.id.radio_group_order);
        backView = (ImageView) findViewById(R.id.btn_back);
        submitView = (Button) findViewById(R.id.btn_submit);
        takePictView1 = (Button) findViewById(R.id.btn_take_pict1);
        takePictView2 = (Button) findViewById(R.id.btn_take_pict2);
        takePictView3 = (Button) findViewById(R.id.btn_take_pict3);
        takePictView1.setVisibility(View.GONE);
        takePictView2.setVisibility(View.GONE);
        takePictView3.setVisibility(View.GONE);

        backView.setOnClickListener(onClick);
        takePictView1.setOnClickListener(onClick);
        takePictView2.setOnClickListener(onClick);
        takePictView3.setOnClickListener(onClick);
        submitView.setOnClickListener(onClick);

        groupView.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int orderId = groupView.getCheckedRadioButtonId();
                RadioButton radioButton = (RadioButton) findViewById(orderId);
                if ((radioButton.getText().toString()).equalsIgnoreCase("iya")) {
                    orderStatus = 1;
                } else if ((radioButton.getText().toString()).equalsIgnoreCase("tidak")) {
                    orderStatus = 2;
                }
                Log.i(TAG, "onCheckedChanged: " + orderStatus);
            }
        });

        accountNameView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() != 0) {
                    takePictView1.setVisibility(View.VISIBLE);
                    takePictView2.setVisibility(View.VISIBLE);
                    takePictView3.setVisibility(View.VISIBLE);
                    PreferenceConnector.writeString(mContext, PreferenceConnector.PHOTO_PATH1, "");
                    PreferenceConnector.writeString(mContext, PreferenceConnector.PHOTO_PATH2, "");
                    PreferenceConnector.writeString(mContext, PreferenceConnector.PHOTO_PATH3, "");
                } else {
                    takePictView1.setVisibility(View.GONE);
                    takePictView2.setVisibility(View.GONE);
                    takePictView3.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void initializeBindService() {
        if (mService == null) {
            bindService(Ekamant.iService, serviceConnection,
                    Context.BIND_AUTO_CREATE);
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder binder) {
            mService = new Messenger(binder);
            // doAPI(apiIndex);
        }
    };

    private void doApi(int apiIndex) {
        try {
            /*JSONObject json = new JSONObject();
            try {
                json.put("file_content", fileContent);
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
            Message msg = Message.obtain(null, apiIndex);

            bundle.putSerializable("parameters", parameters);
            msg.setData(bundle);
            msg.replyTo = mMessenger;
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void sendDataLogin() {
        parameters = new HashMap<String, String>();

        parameters.put("api_key", PreferenceConnector.readString(
                mContext, PreferenceConnector.API_KEY, ""));
        parameters.put("account_id", "43");
        parameters.put("latitude", latitude);
        parameters.put("longitude", longitude);
        parameters.put("notes", notes);
        //parameters.put("file_name", fileName);
        parameters.put("order_status", String.valueOf(orderStatus));
        apiIndex = ApiParam.API_035;

        doApi(apiIndex);
    }

    private static class SubmitHandler extends Handler {
        WeakReference<TaskAddActivity> screen;

        public SubmitHandler(TaskAddActivity myScreen) {
            screen = new WeakReference<TaskAddActivity>(myScreen);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handleMessage: " + msg.toString());

            switch (msg.what) {
                case ApiParam.API_035:
                    screen.get().getResponseLogin(msg);
                    break;
                default:
                    break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    private void getResponseLogin(Message msg) {
        HashMap<Object, Object> response =
                (HashMap<Object, Object>) msg.getData().getSerializable("response");

        Integer result = (Integer) response.get("result");
        switch (result) {
            case ApiParam.API_OK:
                TaskDataSource orderSource = new TaskDataSource(mContext);
                orderSource.open();
                orderSource.insertTask(43, customerName, notes, String.valueOf(orderStatus),
                        latitude, longitude, createdAt);
                orderSource.close();
                startActivity(new Intent(TaskAddActivity.this, DashboardActivity.class));
                finish();
                break;
            case ApiParam.API_NG:
                mLoading.dismiss();
                Toast.makeText(mContext, R.string.error_login, Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    private void getLocation() {

        GpsTracker gpsTracker = new GpsTracker(mContext);
        if (gpsTracker.canGetLocation()) {
            latitude = String.valueOf(gpsTracker.latitude);

            longitude = String.valueOf(gpsTracker.longitude);

            Log.i("AAA", "longitude : " + latitude + ", latituded : " + longitude);
        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }
    }

    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(c.getTime());
    }

    private void saveData() {
        TaskDataSource orderSource = new TaskDataSource(mContext);
        orderSource.open();
        orderSource.insertTask(43, customerName, notes, String.valueOf(orderStatus),
                latitude, longitude, createdAt);
        orderSource.close();
        startActivity(new Intent(TaskAddActivity.this, SignatureActivity.class));
        finish();
    }

    View.OnClickListener onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {

                case R.id.btn_take_pict1:
                    PreferenceConnector.writeString(mContext,
                            PreferenceConnector.PHOTO, "1");
                    selectImage();
                    break;

                case R.id.btn_take_pict2:
                    PreferenceConnector.writeString(mContext,
                            PreferenceConnector.PHOTO, "2");
                    selectImage();
                    break;

                case R.id.btn_take_pict3:
                    PreferenceConnector.writeString(mContext,
                            PreferenceConnector.PHOTO, "3");
                    selectImage();
                    break;

                case R.id.btn_submit:
                    customerName = accountNameView.getText().toString();
                    notes = notesView.getText().toString();
                    createdAt = getCurrentDate();

                    if (customerName.equals("")) {
                        Toast.makeText(mContext, "Please select customer!", Toast.LENGTH_SHORT)
                                .show();
                    } else if (notes.equals("")) {
                        Toast.makeText(mContext, "Please fill notes!", Toast.LENGTH_SHORT).show();
                    } else if (orderStatus == 0) {
                        Toast.makeText(mContext, "Please select order!", Toast.LENGTH_SHORT).show();
                    } else if (takePictView1.isEnabled() && takePictView2.isEnabled()
                            && takePictView3.isEnabled()) {
                        Toast.makeText(
                                mContext, "Please take a photo!", Toast.LENGTH_SHORT).show();
                    } else {
                        saveData();
                    }
                    break;

                case R.id.btn_back:
                    startActivity(new Intent(TaskAddActivity.this, DashboardActivity.class));
                    finish();
                    break;
            }
        }
    };


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(TaskAddActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

        myBase64Image = encodeToBase64(thumbnail, Bitmap.CompressFormat.JPEG, 100);

        String photo = PreferenceConnector.readString(mContext, PreferenceConnector.PHOTO, "");

        if (photo.equals("1")) {
            takePictView1.setEnabled(false);
            takePictView1.setBackgroundColor(getResources().getColor(R.color.green_600));
            PreferenceConnector.writeString(mContext, PreferenceConnector.PHOTO_PATH1, myBase64Image);
        } else if (photo.equals("2")) {
            takePictView2.setEnabled(false);
            takePictView2.setBackgroundColor(getResources().getColor(R.color.green_600));
            PreferenceConnector.writeString(mContext, PreferenceConnector.PHOTO_PATH2, myBase64Image);
        } else if (photo.equals("3")) {
            takePictView3.setEnabled(false);
            takePictView3.setBackgroundColor(getResources().getColor(R.color.green_600));
            PreferenceConnector.writeString(mContext, PreferenceConnector.PHOTO_PATH3, myBase64Image);
        }

    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(TaskAddActivity.this, DashboardActivity.class));
        finish();
    }
}
