/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.network.api;

import android.os.Bundle;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sales1crm.ekamant.sales1crm.utils.Ekamant;
import com.sales1crm.ekamant.sales1crm.utils.PreferenceConnector;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApiRequest {
	private final static String TAG = ApiRequest.class.getSimpleName();

	// I think this is still GET Request
	@SuppressWarnings("unchecked")
	public HashMap<Object, Object> getAPIRequest(
			int apiIndex, HashMap<String, String> parameters,
			boolean hasMultiEntity, Bundle bundle) {
		HashMap<Object, Object> result = new HashMap<Object, Object>();
		try {
			Log.i(TAG, ApiParam.urlBuilder(apiIndex, parameters));

			Log.i(TAG, "MULTI ENTITY : " + hasMultiEntity);
			HttpResponse response;

			HttpClient myClient = new DefaultHttpClient();

			Log.i(TAG,
					"param CONNECTION_TIMEOUT: "
							+ myClient.getParams().getParameter(
							HttpConnectionParams.CONNECTION_TIMEOUT));
			Log.i(TAG,
					"param SO_TIMEOUT: "
							+ myClient.getParams().getParameter(
							HttpConnectionParams.SO_TIMEOUT));

			HttpPost myConnection = new HttpPost(ApiParam.urlBuilder(apiIndex,
					parameters));
			// TODO BUAT MULTIPART ENTITY
			if (hasMultiEntity) {
				Log.i(TAG, "MASUK ME");

				String[] arrKey = bundle.getStringArray("key_string");
				String file_type = bundle.getString("type_file");
				boolean from_verify = false;
				if (bundle.containsKey("verify")) {
					from_verify = bundle.getBoolean("verify");
					Log.i(TAG, "verify : " + from_verify);
				}
				Log.i(TAG, "file_type : " + file_type);
				if (file_type.equalsIgnoreCase(".png")) {
					Log.i(TAG, "cuma gambar");
					MultipartEntity mpEntity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);
					for (int i = 0; i < arrKey.length; i++) {
						Log.i(TAG, "arrkey : " + arrKey[i]);
						Log.i(TAG,
								"arrkey : " + bundle.getString(arrKey[i] + i));
						if (arrKey[i].toString().length() > 0) {
							byte[] bytearr = bundle.getByteArray(arrKey[i]);

							Log.i(TAG, "verify : " + bytearr);
							if (bytearr != null) {
								if (from_verify) {
									Log.i(TAG, "verify : test ");
									mpEntity.addPart(
											arrKey[i],
											new ByteArrayBody(bytearr, bundle
													.getString(arrKey[i] + i)));
									Log.i(TAG,
											"mpentity : " + mpEntity.hashCode());
								} else {
									mpEntity.addPart(
											arrKey[i],
											new ByteArrayBody(bundle
													.getByteArray(arrKey[i]),
													arrKey[i] + file_type));
								}
							}
						}
					}
					myConnection.setEntity(mpEntity);
				} else {
					MultipartEntity mpEntity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);
					Log.i(TAG, "ME : mpEntity : " + mpEntity);
					for (int i = 0; i < arrKey.length; i++) {
						Log.i(TAG, "arrkey : " + arrKey[i]);
						if (arrKey[i].toString().length() > 0) {
							File file = new File(
									bundle.getStringArray(arrKey[i])[1]);
							Log.i(TAG, "ME : arrKey[i] : " + arrKey[i]);
							FileBody fileBody = new FileBody(file,
									bundle.getStringArray(arrKey[i])[2]);
							Log.i(TAG,
									"key : "
											+ bundle.getStringArray(arrKey[i])[0]);
							mpEntity.addPart(
									bundle.getStringArray(arrKey[i])[0],
									fileBody);
						}
					}
					myConnection.setEntity(mpEntity);
				}
			}
			try {
				response = myClient.execute(myConnection);
				String JSONString = EntityUtils.toString(response.getEntity(),
						"UTF-8");
				Log.i(TAG, "jsonstring : " + JSONString);
				if (isValidJSON(JSONString)) {
					result = new ObjectMapper().readValue(JSONString,
							HashMap.class);
					Log.i(TAG, "result : " + result.toString());
					if (result.get("result").equals("OK")) {
						result.put("result", ApiParam.API_OK);
					} else if (result.get("result").equals("NG")) {
						result.put("result", ApiParam.API_NG);
					} else {
						// TODO INVALID VALUE
					}
				} else {
					// TODO INVALID JSON
				}

			} catch (ClientProtocolException e) {
				result.put("result", ApiParam.API_NG);
				e.printStackTrace();
			} catch (IOException e) {
				result.put("result", ApiParam.API_NG);
				e.printStackTrace();
			}
		} catch (Exception e) {
			result.put("result", ApiParam.API_NG);
			e.printStackTrace();
		}
		Log.i(TAG, "result balikan : " + result);
		return result;
	}

	// for api request using POST
	@SuppressWarnings("unchecked")
	public HashMap<Object, Object> getAPIRequest(int apiIndex,
												 HashMap<String, String> parameters, boolean hasMultiEntity,
												 Bundle bundle, boolean for_httpost) {
		HashMap<Object, Object> result = new HashMap<Object, Object>();
		try {
			// Log.i(TAG, APIParam.urlBuilder(apiIndex, parameters));
			HttpResponse response;
			HttpClient myClient = new DefaultHttpClient();
			String url = PreferenceConnector.readString(Ekamant.context,
					PreferenceConnector.URL, "");
			Log.i(TAG, "url : " + (url + ApiParam.getApiUrl(apiIndex)));
			HttpPost myConnection = new HttpPost(url
					+ ApiParam.getApiUrl(apiIndex));

			MultipartEntity mpEntity = new MultipartEntity(
					HttpMultipartMode.BROWSER_COMPATIBLE);
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			for (Map.Entry<String, String> entry : parameters.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();
				Log.i(TAG, "key : " + key);
				Log.i(TAG, "value : " + value);
				// Add your data
				// nameValuePairs.add(new BasicNameValuePair(key, value));
				Charset chars = Charset.forName("UTF-8");
				mpEntity.addPart(key, new StringBody(value, chars));
				// nameValuePairs.add(new BasicNameValuePair("api_key",
				// "53e439ad505274.09616839"));
				// api key : 53e439ad505274.09616839
				//
			}

			// TODO BUAT MULTIPART ENTITY
			if (hasMultiEntity) {
				Log.i(TAG, "MASUK ME");

				String[] arrKey = bundle.getStringArray("key_string");
				String file_type = bundle.getString("type_file");
				Log.i(TAG, "file_type : " + file_type);
				if (file_type.equalsIgnoreCase(".png")) {
					Log.i(TAG, "cuma gambar");
					for (int i = 0; i < arrKey.length; i++) {
						Log.i(TAG, "arrkey : " + arrKey[i]);
						if (arrKey[i].toString().length() > 0) {
							byte[] bytearr = bundle.getByteArray(arrKey[i]);
							Log.i(TAG, "bytearr : " + bytearr);
							if (bytearr != null) {
								mpEntity.addPart(arrKey[i], new ByteArrayBody(
										bundle.getByteArray(arrKey[i]),
										arrKey[i] + file_type));
							}
						}
					}
				} else {
					Log.i(TAG, "arrKey[0] : " + arrKey[0]);
					Log.i(TAG, "key0 : "
							+ bundle.getStringArray(arrKey[0])[0]);
					Log.i(TAG, "path0 : "
							+ bundle.getStringArray(arrKey[0])[1]);
					Log.i(TAG,
							"mimetype0 : "
									+ bundle.getStringArray(arrKey[0])[2]);
					for (int i = 0; i < arrKey.length; i++) {
						Log.i(TAG, "arrkey : " + arrKey[i]);
						if (arrKey[i].toString().length() > 0) {
							File file = new File(
									bundle.getStringArray(arrKey[i])[1]);
							Log.i(TAG, "ME : arrKey[i] : " + arrKey[i]);
							FileBody fileBody = new FileBody(file,
									bundle.getStringArray(arrKey[i])[2]);
							Log.i(TAG,
									"key : "
											+ bundle.getStringArray(arrKey[i])[0]);
							Log.i(TAG,
									"path : "
											+ bundle.getStringArray(arrKey[i])[1]);
							Log.i(TAG,
									"mimetype : "
											+ bundle.getStringArray(arrKey[i])[2]);
							Log.i(TAG, "file : " + file.getPath());
							Log.i(TAG, "filebody : " + fileBody);
							mpEntity.addPart(
									bundle.getStringArray(arrKey[i])[0],
									fileBody);
						}
					}
				}
			}
			myConnection.setEntity(mpEntity);
			try {
				response = myClient.execute(myConnection);
				String JSONString = EntityUtils.toString(response.getEntity(),
						"UTF-8");
				Log.i(TAG, "jsonstring : " + JSONString);
				if (isValidJSON(JSONString)) {
					result = new ObjectMapper().readValue(JSONString,
							HashMap.class);
					if (result.get("result").equals("OK")) {
						result.put("result", ApiParam.API_OK);
					} else if (result.get("result").equals("NG")) {
						result.put("result", ApiParam.API_NG);
					} else {
						// TODO INVALID VALUE
					}
				} else {
					// TODO INVALID JSON
				}

			} catch (ClientProtocolException e) {
				result.put("result", ApiParam.API_NG);
				e.printStackTrace();
			} catch (IOException e) {
				result.put("result", ApiParam.API_NG);
				e.printStackTrace();
			}
		} catch (Exception e) {
			result.put("result", ApiParam.API_NG);
			e.printStackTrace();
		}
		Log.i(TAG, "result balikan : " + result);
		return result;
	}

	// GZIP ENCODING
    /*
     * public HashMap<Object, Object> getAPIRequest(int apiIndex,
	 * HashMap<String, String> parameters, boolean hasMultiEntity, Bundle
	 * bundle) { HashMap<Object, Object> result = new HashMap<Object, Object>();
	 * try { Log.i(TAG, APIParam.urlBuilder(apiIndex, parameters));
	 * HttpResponse response; HttpClient myClient = new DefaultHttpClient(); //
	 * HttpPost myConnection = new HttpPost(APIParam.urlBuilder(apiIndex, //
	 * parameters)); // HttpUriRequest myConnection = new
	 * HttpGet(APIParam.urlBuilder(apiIndex, // parameters)); //
	 * myConnection.addHeader("Accept-Encoding", "gzip"); // HttpGet
	 * myConnection = new HttpGet(APIParam.urlBuilder(apiIndex, // parameters));
	 * HttpUriRequest myConnection = new HttpGet(APIParam.urlBuilder(apiIndex,
	 * parameters));
	 *
	 * // you have to tell the server that you can handle zipped response
	 * myConnection.addHeader("Accept-Encoding", "gzip"); // TODO BUAT MULTIPART
	 * ENTITY if (hasMultiEntity) { Log.i(TAG, "MASUK ME"); MultipartEntity
	 * mpEntity = new MultipartEntity( HttpMultipartMode.BROWSER_COMPATIBLE);
	 * String[] arrKey = bundle.getStringArray("key_string"); for (int i = 0; i
	 * < arrKey.length; i++) { Log.i(TAG, "arrkey : "+arrKey[i]); if
	 * (arrKey[i].toString().length() > 0) { mpEntity.addPart( arrKey[i], new
	 * ByteArrayBody(bundle .getByteArray(arrKey[i]), arrKey[i] + ".png")); } }
	 * ((HttpResponse) myConnection).setEntity(mpEntity); } try { response =
	 * myClient.execute(myConnection);
	 *
	 * InputStream instream = response.getEntity().getContent();
	 * org.apache.http.Header contentEncoding =
	 * response.getFirstHeader("Content-Encoding");
	 *
	 * String test = ""; JSONObject json = null; if (contentEncoding != null &&
	 * contentEncoding.getValue().equalsIgnoreCase("gzip")) { BufferedReader rd
	 * = new BufferedReader(new InputStreamReader(new
	 * GZIPInputStream(instream))); // String jsonText =
	 * org.apache.commons.io.IOUtils.toString(rd); // if
	 * (jsonText.contains("</div>")) { // json = new
	 * JSONObject(jsonText.split("</div>")[1]); // } else { // json = new
	 * JSONObject(jsonText); // } StringBuilder sb = new StringBuilder();
	 *
	 * String line = null; try { while ((line = rd.readLine()) != null) {
	 * sb.append(line + "\n"); } } catch (IOException e) { e.printStackTrace();
	 * } finally { try { instream.close(); } catch (IOException e) {
	 * e.printStackTrace(); } } test = sb.toString(); Log.i(TAG,
	 * "aa : "+sb.toString()); }
	 *
	 * // String JSONString = EntityUtils.toString(response.getEntity(), //
	 * "UTF-8"); if (isValidJSON(test)) { Log.i(TAG, "aa2 : "+test); result =
	 * new ObjectMapper().readValue(test, HashMap.class); if
	 * (result.get("result").equals("OK")) { result.put("result",
	 * APIParam.API_OK); } else if (result.get("result").equals("NG")) {
	 * result.put("result", APIParam.API_NG); } else { // TODO INVALID VALUE } }
	 * else { // TODO INVALID JSON }
	 *
	 * } catch (ClientProtocolException e) { result.put("result",
	 * APIParam.API_NG); e.printStackTrace(); } catch (IOException e) {
	 * result.put("result", APIParam.API_NG); e.printStackTrace(); } } catch
	 * (Exception e) { result.put("result", APIParam.API_NG);
	 * e.printStackTrace(); } Log.i(TAG, "result balikan : "+result); return
	 * result; }
	 */

	@SuppressWarnings("unchecked")
	public HashMap<Object, Object> getAPIRequest(int apiIndex, JSONObject json,
												 HashMap<String, String> parameters, boolean hasMultiEntity,
												 Bundle bundle) {
		HashMap<Object, Object> result = new HashMap<Object, Object>();
		try {
			Log.i(TAG, ApiParam.urlBuilder(apiIndex, parameters));
			HttpResponse response;
			HttpClient myClient = new DefaultHttpClient();
			HttpPost myConnection = new HttpPost(ApiParam.urlBuilder(apiIndex,
					parameters));
			Log.i(TAG, "JSON " + json.toString());
			myConnection.setHeader("json", json.toString());
			StringEntity se = new StringEntity(json.toString(), "UTF-8");
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			myConnection.setEntity(se);
			// TODO BUAT MULTIPART ENTITY
			if (hasMultiEntity) {
				Log.i(TAG, "MASUK ME");

				String[] arrKey = bundle.getStringArray("key_string");
				String file_type = bundle.getString("type_file");
				Log.i(TAG, "file_type : " + file_type);
				if (file_type.equalsIgnoreCase(".png")) {
					Log.i(TAG, "cuma gambar");
					MultipartEntity mpEntity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);
					for (int i = 0; i < arrKey.length; i++) {
						Log.i(TAG, "arrkey : " + arrKey[i]);
						if (arrKey[i].toString().length() > 0) {
							mpEntity.addPart(arrKey[i], new ByteArrayBody(
									bundle.getByteArray(arrKey[i]), arrKey[i]
									+ file_type));
						}
					}
					myConnection.setEntity(mpEntity);
				} else {
					MultipartEntity mpEntity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);
					Log.i(TAG, "ME : mpEntity : " + mpEntity);
					for (int i = 0; i < arrKey.length; i++) {
						Log.i(TAG, "arrkey : " + arrKey[i]);
						if (arrKey[i].toString().length() > 0) {
							File file = new File(
									bundle.getStringArray(arrKey[i])[1]);
							Log.i(TAG, "ME : arrKey[i] : " + arrKey[i]);
							FileBody fileBody = new FileBody(file,
									bundle.getStringArray(arrKey[i])[2]);
							Log.i(TAG,
									"key : "
											+ bundle.getStringArray(arrKey[i])[0]);
							mpEntity.addPart(
									bundle.getStringArray(arrKey[i])[0],
									fileBody);
						}
					}
					myConnection.setEntity(mpEntity);
				}
			}
			try {
				response = myClient.execute(myConnection);
				String JSONString = EntityUtils.toString(response.getEntity(),
						"UTF-8");
				Log.i(TAG, "Response " + JSONString);
				if (isValidJSON(JSONString)) {
					result = new ObjectMapper().readValue(JSONString,
							HashMap.class);
					if (result.get("result").equals("OK")) {
						result.put("result", ApiParam.API_OK);
					} else if (result.get("result").equals("NG")) {
						result.put("result", ApiParam.API_NG);
					} else {
						// TODO INVALID VALUE
					}
				} else {
					// TODO INVALID JSON
				}

			} catch (ClientProtocolException e) {
				result.put("result", ApiParam.API_NG);
				e.printStackTrace();
			} catch (IOException e) {
				result.put("result", ApiParam.API_NG);
				e.printStackTrace();
			}
		} catch (Exception e) {
			result.put("result", ApiParam.API_NG);
			e.printStackTrace();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public HashMap<Object, Object> getAPIRequest(int apiIndex, JSONArray json,
												 HashMap<String, String> parameters, boolean hasMultiEntity,
												 Bundle bundle) {
		HashMap<Object, Object> result = new HashMap<Object, Object>();
		try {
			Log.i(TAG, ApiParam.urlBuilder(apiIndex, parameters));
			HttpResponse response;
			HttpClient myClient = new DefaultHttpClient();
			HttpPost myConnection = new HttpPost(ApiParam.urlBuilder(apiIndex,
					parameters));
			Log.i(TAG, "JSON " + json.toString());
			myConnection.setHeader("json", json.toString());
			StringEntity se = new StringEntity(json.toString(), "UTF-8");
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			myConnection.setEntity(se);
			// TODO BUAT MULTIPART ENTITY
			if (hasMultiEntity) {
				Log.i(TAG, "MASUK ME");

				String[] arrKey = bundle.getStringArray("key_string");
				String file_type = bundle.getString("type_file");
				Log.i(TAG, "file_type : " + file_type);
				if (file_type.equalsIgnoreCase(".png")) {
					Log.i(TAG, "cuma gambar");
					MultipartEntity mpEntity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);
					for (int i = 0; i < arrKey.length; i++) {
						Log.i(TAG, "arrkey : " + arrKey[i]);
						if (arrKey[i].toString().length() > 0) {
							mpEntity.addPart(arrKey[i], new ByteArrayBody(
									bundle.getByteArray(arrKey[i]), arrKey[i]
									+ file_type));
						}
					}
					myConnection.setEntity(mpEntity);
				} else {
					MultipartEntity mpEntity = new MultipartEntity(
							HttpMultipartMode.BROWSER_COMPATIBLE);
					Log.i(TAG, "ME : mpEntity : " + mpEntity);
					for (int i = 0; i < arrKey.length; i++) {
						Log.i(TAG, "arrkey : " + arrKey[i]);
						if (arrKey[i].toString().length() > 0) {
							File file = new File(
									bundle.getStringArray(arrKey[i])[1]);
							Log.i(TAG, "ME : arrKey[i] : " + arrKey[i]);
							FileBody fileBody = new FileBody(file,
									bundle.getStringArray(arrKey[i])[2]);
							Log.i(TAG,
									"key : "
											+ bundle.getStringArray(arrKey[i])[0]);
							mpEntity.addPart(
									bundle.getStringArray(arrKey[i])[0],
									fileBody);
						}
					}
					myConnection.setEntity(mpEntity);
				}
			} else {

			}
			try {
				response = myClient.execute(myConnection);
				String JSONString = EntityUtils.toString(response.getEntity(),
						"UTF-8");
				Log.i(TAG, "Response " + JSONString);
				if (isValidJSON(JSONString)) {
					result = new ObjectMapper().readValue(JSONString,
							HashMap.class);
					if (result.get("result").equals("OK")) {
						result.put("result", ApiParam.API_OK);
					} else if (result.get("result").equals("NG")) {
						result.put("result", ApiParam.API_NG);
					} else {
						// TODO INVALID VALUE
					}
				} else {
					// TODO INVALID JSON
				}

			} catch (ClientProtocolException e) {
				result.put("result", ApiParam.API_NG);
				e.printStackTrace();
			} catch (IOException e) {
				result.put("result", ApiParam.API_NG);
				e.printStackTrace();
			}
		} catch (Exception e) {
			result.put("result", ApiParam.API_NG);
			e.printStackTrace();
		}
		return result;
	}

	public boolean isValidJSON(final String json) {
		boolean valid = false;
		String jsonConvert = "";
		try {
			Log.i(TAG, json);
			try {
				JSONObject object = new JSONObject(json);
				if (object.getJSONObject("datas").has("accounts")) {
					JSONArray jsonArray = object.getJSONObject("datas").optJSONArray("accounts");
					for (int i = 0; i < jsonArray.length(); i++) {
						if (jsonArray.getJSONObject(i).getString("type_id").equals("")) {
							Log.i(TAG, "type_id : " + jsonArray.getJSONObject(i).getString("type_id"));
							jsonArray.getJSONObject(i).put("type_id", "1");
							Log.i(TAG, "type_id : " + jsonArray.getJSONObject(i).getString("type_id"));
						}
					}
					object.getJSONObject("datas").put("accounts", jsonArray);
				}
				jsonConvert = object.toString();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.i(TAG, "jsonparser : " + e.getMessage());
			}

			Log.i(TAG, "jsonparser : " + jsonConvert);
			final JsonParser parser = new ObjectMapper().getFactory()
					.createParser(jsonConvert);
			while (parser.nextToken() != null) {
			}
			valid = true;
		} catch (JsonParseException jpe) {
			Log.i(TAG, jpe.getMessage());
		} catch (IOException ioe) {
			Log.i(TAG, ioe.getMessage());
		}

		return valid;
	}
}
