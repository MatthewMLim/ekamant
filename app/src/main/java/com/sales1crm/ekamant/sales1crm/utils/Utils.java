/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.utils;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;

import com.sales1crm.ekamant.sales1crm.utils.imageloader.ImageLoader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {

    public static ImageLoader imageLoader;
    public static DecimalFormat formatter;

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    public static String formatNumber(int number) {
        formatter = new DecimalFormat("#,###,###");
        return formatter.format(number);
    }

    public static String formatNumberLong(long number) {
        formatter = new DecimalFormat("#,###,###");
        return formatter.format(number);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    /**
     * This method converts device specific pixels to density independent
     * pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    public static String[] typeData() {
        String[] type_data = {"Type", "None", "New Business",
                "Existing Business"};
        return type_data;
    }

    public static Bitmap getroundBitmap(Bitmap bitmap) {
        Bitmap output = null;
        // if (bitmap.getWidth() < bitmap.getHeight()) {
        // output = Bitmap.createBitmap(bitmap.getWidth(),
        // bitmap.getWidth(), Config.ARGB_8888);
        // } else if(bitmap.getWidth() > bitmap.getHeight()) {
        // output = Bitmap.createBitmap(bitmap.getHeight(),
        // bitmap.getHeight(), Config.ARGB_8888);
        // }
        if (bitmap.getWidth() >= bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap,
                    bitmap.getWidth() / 2 - bitmap.getHeight() / 2, 0,
                    bitmap.getHeight(), bitmap.getHeight());
        } else {

            output = Bitmap.createBitmap(bitmap, 0, bitmap.getHeight() / 2
                            - bitmap.getWidth() / 2, bitmap.getWidth(),
                    bitmap.getWidth());
        }
        Bitmap drawableBitmap = output.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(drawableBitmap);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        float roundPx = 0;
        if (bitmap.getWidth() < bitmap.getHeight()) {
            roundPx = bitmap.getHeight() / 2;
        } else {
            roundPx = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap getrectBitmap(Bitmap bitmap) {
        Bitmap output = null;
        // if (bitmap.getWidth() < bitmap.getHeight()) {
        // output = Bitmap.createBitmap(bitmap.getWidth(),
        // bitmap.getWidth(), Config.ARGB_8888);
        // } else if(bitmap.getWidth() > bitmap.getHeight()) {
        // output = Bitmap.createBitmap(bitmap.getHeight(),
        // bitmap.getHeight(), Config.ARGB_8888);
        // }
        if (bitmap.getWidth() >= bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap,
                    bitmap.getWidth() / 2 - bitmap.getHeight() / 2, 0,
                    bitmap.getHeight(), bitmap.getHeight());
        } else {

            output = Bitmap.createBitmap(bitmap, 0, bitmap.getHeight() / 2
                            - bitmap.getWidth() / 2, bitmap.getWidth(),
                    bitmap.getWidth());
        }
        Bitmap drawableBitmap = output.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(drawableBitmap);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        float roundPx = 0;
        if (bitmap.getWidth() < bitmap.getHeight()) {
            roundPx = bitmap.getHeight() / 10;
        } else {
            roundPx = bitmap.getWidth() / 10;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static android.graphics.BitmapFactory.Options getSize(Context c,
                                                                 int resId) {
        android.graphics.BitmapFactory.Options o = new android.graphics.BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(c.getResources(), resId, o);
        return o;
    }

    public static Bitmap scaleImage(Context context, Uri photoUri)
            throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();

        int orientation = getOrientation(context, photoUri);

        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);

        // // Create the bitmap from file
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;
        // srcBitmap = BitmapFactory.decodeStream(is, null, options);
        // } else {
        srcBitmap = BitmapFactory.decodeStream(is, null, options);
        // }
        is.close();

		/*
         * if the orientation is not 0 (or -1, which means we don't know), we
		 * have to do a rotation.
		 */
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);

            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0,
                    srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
        }

        String type = context.getContentResolver().getType(photoUri);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (type.equals("image/png")) {
            srcBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        } else if (type.equals("image/jpg") || type.equals("image/jpeg")) {
            srcBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        }
        byte[] bMapArray = baos.toByteArray();
        baos.close();
        return BitmapFactory.decodeByteArray(bMapArray, 0, bMapArray.length);
    }

    public static int getOrientation(Context context, Uri photoUri) {
        /* it's on the external media. */
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION},
                null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }

        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    /**
     * ------------ Helper Methods ----------------------
     * */

    /**
     * Creating file uri to store image/video
     */
    public static Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * returning image / video
     */
    private static String IMAGE_DIRECTORY_NAME = "MSF";

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    /**
     * returning folder where file will be saved
     */
    private static String FILE_DIRECTORY_NAME = "MSF";
    public static String FILE_LOCATION_NAME = "location.txt";

    public static File createFileFolder() {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStorageDirectory(), FILE_DIRECTORY_NAME);

        Log.i("AAA", "mediaStorageDir : " + mediaStorageDir.getPath());

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        return mediaStorageDir;
    }

    public static File createFile(File filepath) {
        File file = new File(filepath.getPath() + File.separator
                + FILE_LOCATION_NAME);
        Log.i("AAA", "FILE_LOCATION_NAME : " + file.getPath());

        return file;
    }

}
