/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.features.authentication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sales1crm.ekamant.sales1crm.R;
import com.sales1crm.ekamant.sales1crm.features.DashboardActivity;
import com.sales1crm.ekamant.sales1crm.features.authentication.service.CheckInService;
import com.sales1crm.ekamant.sales1crm.features.shared.customview.CustomProgressDialog;
import com.sales1crm.ekamant.sales1crm.network.api.ApiParam;
import com.sales1crm.ekamant.sales1crm.network.api.EkamantApiService;
import com.sales1crm.ekamant.sales1crm.utils.Ekamant;
import com.sales1crm.ekamant.sales1crm.utils.PermissionCheck;
import com.sales1crm.ekamant.sales1crm.utils.PreferenceConnector;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Add a class Header comment!
 */

public class CheckInActivity extends AppCompatActivity {

    public static final String TAG = CheckInActivity.class.getSimpleName();

    private int apiIndex;
    private int checkIn = 1;
    private int hours, minutes, seconds, currenttime, TimerTime, startTime;
    private boolean isCheckout = false;
    private boolean hasService = false;

    private HashMap<String, String> parameters;

    private TextView tvCountdownCheckin, tvNotice, tvClock, tvAM;
    private ImageView ivLogout;
    private Button btnCheckin;
    private CheckBox cbRemindMe;

    private Context mContext;
    private PendingIntent pendingIntent;
    private AlarmManager am;
    private Thread currentTimeThread = null;

    private CustomProgressDialog mLoading;
    private MyCountDownTimer countdowntimer = null;

    private Messenger mService = null;
    private Messenger mMessenger = new Messenger(new CheckIn(this));

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_checkin);

        this.mContext = this;
        Ekamant.context = this;
        mLoading = CustomProgressDialog.createDialog(this, "", "");
        mLoading.setCancelable(false);

        if (Ekamant.iService == null) {
            Ekamant.iService = new Intent(CheckInActivity.this,
                    EkamantApiService.class);
            startService(Ekamant.iService);
            Ekamant.directory = "Android/data/"
                    + getApplicationContext().getPackageName() + "/";
        }

        Intent intent = new Intent(mContext, CheckInService.class);
        intent.putExtra("alarm", true);
        pendingIntent = PendingIntent.getService(mContext,
                Ekamant.PENDINGINTENT_LATEALARM, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);

        // alarm = new WakeAlarm();

        checkIn = PreferenceConnector.readInt(mContext,
                PreferenceConnector.CHECKIN, Ekamant.DEFAULT_VALUEFORCHECK_IN);

        try {
            isCheckout = getIntent().getExtras().getBoolean("is_checkout");
        } catch (Exception e) {
            // TODO: handle exception
        }

        initView();
        startClockOfCurrenttime();
        convertTime();

    }

    @Override
    protected void onDestroy() {
        if (hasService) {
            mContext.unbindService(serviceConnection);
            mContext.stopService(Ekamant.iService);
        }
        countdowntimer.cancel();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        countdowntimer.cancel();
        finish();
    }

    private void initializeBindService() {
        if (mService == null) {
            hasService = true;
            bindService(Ekamant.iService, serviceConnection,
                    Context.BIND_AUTO_CREATE);
        }
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName componentName,
                                       IBinder binder) {
            mService = new Messenger(binder);
            doAPI(apiIndex);
        }
    };

    private void doAPI(int apiIndex) {
        // progressHUD = ProgressHUD.show(context, "", true, false, null);
        try {
            Message msg = Message.obtain(null, apiIndex);
            Bundle bundle = new Bundle();
            bundle.putSerializable("parameters", parameters);
            msg.setData(bundle);
            msg.replyTo = mMessenger;
            mService.send(msg);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private static class CheckIn extends Handler {
        WeakReference<CheckInActivity> screen;

        public CheckIn(CheckInActivity myScreen) {
            screen = new WeakReference<CheckInActivity>(myScreen);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, msg.toString());
            switch (msg.what) {
                case ApiParam.API_004:
                    screen.get().checkIn(msg);
                    break;
                case ApiParam.API_002:
                    screen.get().doLogout(msg);
                    break;
                default:
                    break;
            }

        }
    }

    @SuppressWarnings("unchecked")
    private void checkIn(Message msg) {
        HashMap<Object, Object> response = (HashMap<Object, Object>) msg
                .getData().getSerializable("response");
        Log.i(TAG, "result : " + response);
        Integer result = (Integer) response.get("result");
        switch (result) {
            case ApiParam.API_OK:
                if (countdowntimer != null) {
                    countdowntimer.cancel();
                }
                am.cancel(pendingIntent);
                HashMap<Object, Object> user_data = (HashMap<Object, Object>) response
                        .get("datas");
                if (PreferenceConnector.readString(mContext,
                        PreferenceConnector.URL, "").contains("domaintest2")) {
                    if (user_data.get("user_id") != null) {
                        PreferenceConnector.writeString(mContext,
                                PreferenceConnector.USER_ID,
                                user_data.get("user_id").toString());
                    }
                }

                //to cancel the currently set alarm for start working, just a precaution
                Intent intent = new Intent(this, CheckInService.class);
                intent.putExtra("alarm_start", true);
                PendingIntent pendingIntent = PendingIntent.getService(this,
                        Ekamant.PENDINGINTENT_STARTALARM, intent,
                        PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager am = (AlarmManager) this
                        .getSystemService(Context.ALARM_SERVICE);
                am.cancel(pendingIntent);

                mLoading.dismiss();
                Intent i = new Intent(CheckInActivity.this, DashboardActivity.class);
                startActivity(i);
                // close this activity
                finish();
                break;
            case ApiParam.API_NG:
                mLoading.dismiss();
                Toast.makeText(mContext, "FAILED TO CHECK IN, oops",
                        Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    @SuppressWarnings("unchecked")
    private void doLogout(Message msg) {
        HashMap<Object, Object> response = (HashMap<Object, Object>) msg
                .getData().getSerializable("response");
        Log.i(TAG, "result : " + response);
        Integer result = (Integer) response.get("result");
        switch (result) {
            case ApiParam.API_OK:
                if (countdowntimer != null) {
                    countdowntimer.cancel();
                }
                // cancelling all alarm, remind me, start work, end work
                Intent intent = new Intent(this, CheckInService.class);
                intent.putExtra("alarm", true);
                PendingIntent pendingIntent = PendingIntent.getService(this,
                        Ekamant.PENDINGINTENT_LATEALARM, intent,
                        PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager am = (AlarmManager) this
                        .getSystemService(Context.ALARM_SERVICE);

                am.cancel(pendingIntent);

                Intent intentStart = new Intent(this, CheckInService.class);
                intentStart.putExtra("alarm_start", true);
                PendingIntent pendingIntentStart = PendingIntent.getService(this,
                        Ekamant.PENDINGINTENT_LATEALARM, intentStart,
                        PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager amStart = (AlarmManager) this
                        .getSystemService(Context.ALARM_SERVICE);

                amStart.cancel(pendingIntentStart);

                Intent intentEnd = new Intent(this, CheckInService.class);
                intentEnd.putExtra("alarm_end", true);
                PendingIntent pendingIntentEnd = PendingIntent.getService(this,
                        Ekamant.PENDINGINTENT_LATEALARM, intentEnd,
                        PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager amEnd = (AlarmManager) this
                        .getSystemService(Context.ALARM_SERVICE);

                amEnd.cancel(pendingIntentEnd);

                mLoading.dismiss();
                Intent i = new Intent(CheckInActivity.this, LoginActivity.class);
                startActivity(i);
                // close this activity
                finish();
                break;
            case ApiParam.API_NG:
                mLoading.dismiss();
                break;
            default:
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case PermissionCheck.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();

                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CALL_PHONE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                } else {
                    // Permission Denied
                    Toast.makeText(CheckInActivity.this, "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();
                }
            }
            break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void checkIn() {
        mLoading.show();
        parameters = new HashMap<String, String>();
        parameters.put("api_key", PreferenceConnector.readString(mContext,
                PreferenceConnector.API_KEY, ""));
        apiIndex = ApiParam.API_004;
        initializeBindService();
    }

    private void Logout() {
        mLoading.show();
        parameters = new HashMap<String, String>();
        parameters.put("api_key", PreferenceConnector.readString(mContext,
                PreferenceConnector.API_KEY, ""));
        apiIndex = ApiParam.API_002;
        initializeBindService();
    }

    private void initView() {
        tvClock = (TextView) findViewById(R.id.tvClock);
        tvAM = (TextView) findViewById(R.id.tvAM);
        tvCountdownCheckin = (TextView) findViewById(R.id.tvCountdownCheckin);
        tvNotice = (TextView) findViewById(R.id.tvNotice);
        ivLogout = (ImageView) findViewById(R.id.ivLogout);
        cbRemindMe = (CheckBox) findViewById(R.id.cbRemindMe);
        cbRemindMe.setOnClickListener(clickListener);
        cbRemindMe.setChecked(PreferenceConnector.readBoolean(mContext,
                PreferenceConnector.REMINDME, false));
        if (cbRemindMe.isChecked()) {
            PreferenceConnector.writeBoolean(mContext,
                    PreferenceConnector.REMINDME, true);
        }
        btnCheckin = (Button) findViewById(R.id.btnCheckin);
        btnCheckin.setOnClickListener(clickListener);

        ivLogout.setOnClickListener(clickListener);
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btnCheckin:
                    //if (isConnected) {
                    if (isCheckout || checkIn == Ekamant.CHECKOUT_ALREADY) {
                        Toast.makeText(mContext, "You already check out for today",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        checkIn();
                        // Intent i = new Intent(CheckInActivity.this,
                        // DashBoardActivity.class);
                        // startActivity(i);
                        // ((Activity) context).overridePendingTransition(
                        // R.anim.abc_fade_in, R.anim.abc_slide_out_top);
                    }
                    /*} else {
                        Toast.makeText(mContext, R.string.error_connection, Toast.LENGTH_SHORT).show();
                    }*/
                    break;
                case R.id.cbRemindMe:
                    Log.i(TAG, "CLICK REMIND ME");
                    if (cbRemindMe.isChecked()) {
                        PreferenceConnector.writeBoolean(mContext,
                                PreferenceConnector.REMINDME, true);
                        // alarm.setAlarm(CheckInActivity.this);
                        setAlarm();
                    } else {
                        PreferenceConnector.writeBoolean(mContext,
                                PreferenceConnector.REMINDME, false);
                        // alarm.cancelAlarm(CheckInActivity.this);
                        Log.i(TAG, "MASUK CANCEL ALARM");
                        am.cancel(pendingIntent);
                    }
                    break;
                case R.id.ivLogout:
                    Logout();
                    break;

                default:
                    break;
            }
        }
    };

    public void setAlarm() {
        Log.i(TAG, "MASUK SET ALARM");
        long _alarm = 0;
        am.cancel(pendingIntent);

        int start_hour = PreferenceConnector.readInt(mContext,
                PreferenceConnector.START_HOUR, 8);
        int start_minute = PreferenceConnector.readInt(mContext,
                PreferenceConnector.START_MINUTE, 15);
        int new_minute = start_minute - 15;
        if (new_minute < 0) {
            start_hour = start_hour - 1;
            start_minute = new_minute + 60;
            Log.i(TAG, "start hour: " + start_hour + "," + start_minute);
        } else {
            Log.i(TAG, "else start hour: " + start_hour + "," + start_minute);
        }

        GregorianCalendar twopm = new GregorianCalendar();
        twopm.set(GregorianCalendar.HOUR_OF_DAY, start_hour);
        twopm.set(GregorianCalendar.MINUTE, start_minute);
        twopm.set(GregorianCalendar.SECOND, 0);
        twopm.set(GregorianCalendar.MILLISECOND, 0);
        if (twopm.before(new GregorianCalendar())) {
            twopm.add(GregorianCalendar.DAY_OF_MONTH, 1);
        }

        Log.i(TAG, "ALARM- timeleft in check in : " + twopm.getTimeInMillis());

        // am.setRepeating(AlarmManager.RTC_WAKEUP, twopm.getTimeInMillis(),
        // DateUtils.DAY_IN_MILLIS, pendingIntent);
        am.set(AlarmManager.RTC_WAKEUP, twopm.getTimeInMillis(), pendingIntent);
    }

    private void startClockOfCurrenttime() {
        Log.i(TAG, "startClockOfCurrenttime start");
        Runnable myRunnableThread = new CountDownRunner();
        currentTimeThread = new Thread(myRunnableThread);
        currentTimeThread.start();
    }

    class CountDownRunner implements Runnable {
        // @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    doWork();
                    Thread.sleep(1000); // Pause of 1 Second
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                } catch (Exception e) {
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void convertTime() {
        Log.i(TAG, "convertTime");
        Date time = Calendar.getInstance().getTime();

        int start_hour = PreferenceConnector.readInt(mContext,
                PreferenceConnector.START_HOUR, 8);
        int start_minute = PreferenceConnector.readInt(mContext,
                PreferenceConnector.START_MINUTE, 15);
        Log.i(TAG, "start hour: " + start_hour + "," + start_minute);
        // time up
        startTime = (start_hour * 3600 * 1000) + (start_minute * 60 * 1000);
        // startTime = 16 * 3600 * 1000+(31*60*1000);
        hours = time.getHours() * 3600;
        minutes = time.getMinutes() * 60;
        seconds = time.getSeconds();
        Log.i(TAG, "hours : " + time.getHours());
        // current time
        currenttime = (hours + minutes + seconds) * 1000;

        // check whether time is already over 8 oclock or not
        if (currenttime - startTime > 0) {
            // time is already over 8oclock
            TimerTime = (24 * 3600 * 1000) - (currenttime - startTime);
            Log.i(TAG, "timertime + : " + TimerTime);
        } else {
            // time havent reach 8oclock
            TimerTime = startTime - currenttime;
            Log.i(TAG, "timer - : " + TimerTime);
        }

        Log.i(TAG, "Timertime : " + TimerTime);
        if (checkIn == 1) {
            if (currenttime - startTime > 0) {
                // time is already over 8oclock
                countdowntimer = new MyCountDownTimer(currenttime - startTime,
                        1000);
                countdowntimer.setIs_late(true);
                countdowntimer.start();
            } else {
                // time havent reach 8oclock
                countdowntimer = new MyCountDownTimer(TimerTime, 1000);
                countdowntimer.start();
            }
        } else {
            countdowntimer = new MyCountDownTimer(TimerTime, 1000);
            countdowntimer.start();
        }
    }

    @SuppressLint("SimpleDateFormat")
    public class MyCountDownTimer extends CountDownTimer {
        private int CountDown;
        private boolean isrunning = false, is_late = false;

        public MyCountDownTimer(int startTime, int interval) {

            super(startTime, interval);
            Log.i(TAG, "enter time : " + startTime);
            String pattern = "HH:mm a";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            Date tryDate = new Date(Long.parseLong(String.valueOf(startTime)));
            String formattedDate = simpleDateFormat.format(tryDate);
            Log.i(TAG, "formattedDate : " + formattedDate);
            Log.i(TAG, "enter time : " + (int) tryDate.getTime());
            this.CountDown = startTime;//(int) tryDate.getTime();
        }

        @Override
        public void onFinish() {
            tvCountdownCheckin.setText("00:00:00");
            cancel();
            isrunning = false;
            is_late = true;

            int start_hour = PreferenceConnector.readInt(mContext,
                    PreferenceConnector.START_HOUR, 8);
            int start_minute = PreferenceConnector.readInt(mContext,
                    PreferenceConnector.START_MINUTE, 15);
            Log.i(TAG, "start_hour : " + start_hour);
            Log.i(TAG, "start_minute : " + start_minute);

            Date time = Calendar.getInstance().getTime();
            startTime = (start_hour * 3600 * 1000) + (start_minute * 60 * 1000);
            hours = time.getHours() * 3600;
            minutes = time.getMinutes() * 60;
            seconds = time.getSeconds();
            Log.i(TAG, "hours : " + time.getHours());
            Log.i(TAG, "startTime : " + startTime);
            // current time
            currenttime = (hours + minutes + seconds) * 1000;
            CountDown = Math.abs(currenttime - startTime);
            start();
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (is_late) {
                tvNotice.setTextColor(getResources().getColor(
                        R.color.login_error_message_red));
                tvNotice.setText("YOU ARE LATE");
                tvCountdownCheckin.setTextColor(getResources().getColor(
                        R.color.login_error_message_red));
            }
            String pattern = "HH:mm a";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            Date tryDate = new Date(Long.parseLong(String.valueOf(CountDown)));
            String formattedDate = simpleDateFormat.format(tryDate);
            Log.i(TAG, "formattedDate : " + formattedDate);
            Log.i(TAG, "enter time : " + (int) tryDate.getTime());
            Log.i(TAG, "CountDown: " + CountDown);
            String Hours = String.valueOf((CountDown / 1000) / 3600);
            Log.i(TAG, "raw hours: " + Hours);
            if (Hours.length() == 1) {
                Hours = "0" + Hours;
            }
            int minutes_left = ((CountDown / 1000) - (Integer.parseInt(Hours) * 3600)) / 60;
            String Minutes = String.valueOf(minutes_left);
            Log.i(TAG, "raw Minutes: " + Minutes);
            if (Minutes.length() == 1) {
                Minutes = "0" + Minutes;
            }
            int seconds_left = (((CountDown / 1000) - (Integer.parseInt(Hours) * 3600)) - minutes_left * 60);
            String Seconds = String.valueOf(seconds_left);
            Log.i(TAG, "raw Minutes: " + Seconds);
            if (Seconds.length() == 1) {
                Seconds = "0" + Seconds;
            }
            tvCountdownCheckin.setText(Hours + ":" + Minutes + ":" + Seconds);

            if (is_late) {
                CountDown += 1000;
            } else {
                CountDown -= 1000;
            }
        }

        public boolean isRunning() {
            return isrunning;
        }

        public boolean isIs_late() {
            return is_late;
        }

        public void setIs_late(boolean is_late) {
            this.is_late = is_late;
        }
    }

    public void doWork() {
        runOnUiThread(new Runnable() {
            @SuppressWarnings("deprecation")
            public void run() {
                try {
                    Calendar c = Calendar.getInstance();
                    int hours = c.get(Calendar.HOUR_OF_DAY);
                    int minutes = c.get(Calendar.MINUTE);

                    String Hours = String.valueOf((hours));
                    if (Hours.length() == 1) {
                        Hours = "0" + Hours;
                    }
                    String Minutes = String.valueOf(minutes);
                    if (Minutes.length() == 1) {
                        Minutes = "0" + Minutes;
                    }

                    String curTime = Hours + ":" + Minutes;

                    int AM_orPM = c.get(Calendar.AM_PM);
                    if (AM_orPM == 0) {
                        tvAM.setText("a.m");
                    } else {
                        tvAM.setText("p.m");
                    }

                    tvClock.setText(curTime);
                } catch (Exception e) {
                }
            }
        });
    }
}
