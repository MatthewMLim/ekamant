/*
 * Copyright 2017 Matthew Lim
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sales1crm.ekamant.sales1crm.features.shared;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sales1crm.ekamant.sales1crm.R;
import com.sales1crm.ekamant.sales1crm.database.task.Task;
import com.sales1crm.ekamant.sales1crm.features.shared.customview.CustomTextView;

import java.util.List;

/**
 * TODO: Add a class Header comment!
 */

public class TaskCustomAdapter extends BaseAdapter {

    private static final String TAG = TaskCustomAdapter.class.getSimpleName();

    private Activity activity;
    private LayoutInflater inflater;
    private List<Task> orderList;

    public TaskCustomAdapter(Activity activity, List<Task> orderList) {
        this.activity = activity;
        this.orderList = orderList;
    }

    @Override
    public int getCount() {
        return orderList.size();
    }

    @Override
    public Object getItem(int location) {
        return orderList.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null) {
            inflater = (LayoutInflater) activity.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null)
            convertView = inflater.inflate(R.layout.order_item, null);

        CustomTextView accountNameView =
                (CustomTextView) convertView.findViewById(R.id.text_account_name);
        CustomTextView notesView =
                (CustomTextView) convertView.findViewById(R.id.text_notes);
        CustomTextView orderStatusView =
                (CustomTextView) convertView.findViewById(R.id.text_order_status);
        CustomTextView taskDateView =
                (CustomTextView) convertView.findViewById(R.id.text_task_date);

        // getting movie data for the row
        Task task = orderList.get(position);

        accountNameView.setText(task.getCustomer_name());
        notesView.setText(task.getNotes());
        taskDateView.setText(task.getCreated_at());

        if (task.getOrder_status().equals("1")) {
            orderStatusView.setText("Order : Yes");
        } else {
            orderStatusView.setText("Order : No");
        }

        return convertView;
    }


}
